package com.imaisnaini.kalenderku.bl.data.remote.request.event

import com.google.gson.annotations.SerializedName

data class DeleteEventReqeust(
    @SerializedName("event_id") val eventID: String
)
