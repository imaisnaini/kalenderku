package com.imaisnaini.kalenderku.bl.data.remote.response.event

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.CommonField
import com.imaisnaini.kalenderku.bl.data.domain.model.UserEvent
import com.imaisnaini.kalenderku.bl.data.remote.response.CommonFieldResponse

data class UserEventResponse(
    @SerializedName("user_event_id")
    val userEventID: String,
    @SerializedName("user_id")
    val userID: String,
    @SerializedName("event_id")
    val eventID: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("commonField")
    val commonFieldResponse: CommonFieldResponse
) {
    fun mapToLocal() = UserEvent(
        userEventID = userEventID,
        userID = userID,
        eventID = eventID,
        status = status,
        commonField = commonFieldResponse.mapToLocal()
    )
}
