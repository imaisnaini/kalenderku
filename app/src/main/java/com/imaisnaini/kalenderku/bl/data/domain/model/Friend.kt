package com.imaisnaini.kalenderku.bl.data.domain.model

data class Friend(
    val friendID: String,
    val userID: String,
    val friendUserID: String,
    val status: String,
    val fullname: String,
    val email: String,
    val phone: String,
    val commonField: CommonField? = null
)