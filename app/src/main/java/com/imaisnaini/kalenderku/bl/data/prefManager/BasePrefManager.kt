package id.bni.maverick.core.util.prefManager

import android.content.SharedPreferences

abstract class BasePrefManager {
    abstract val sharedPreferences: SharedPreferences

    @Synchronized
    fun setString(key: String, value: String?) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    @Synchronized
    fun setStringSet(key: String, value: Set<String>) {
        sharedPreferences.edit().putStringSet(key, value).apply()
    }

    fun appendStringSet(key: String, value: String) = getStringSet(key).takeIf { it.isNotEmpty() }
        ?.let { sharedPreferences.edit().putStringSet(key, it + value).apply() }
        ?: sharedPreferences.edit().putStringSet(key, setOf(value)).apply()

    @Synchronized
    fun setInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    @Synchronized
    fun setLong(key: String, value: Long) {
        sharedPreferences.edit().putLong(key, value).apply()
    }

    @Synchronized
    fun setFloat(key: String, value: Float) {
        sharedPreferences.edit().putFloat(key, value).apply()
    }

    @Synchronized
    fun setBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    @Synchronized
    fun getString(key: String): String? = sharedPreferences.getString(key, null)

    @Synchronized
    fun getStringSet(key: String) = sharedPreferences.getStringSet(key, emptySet()).orEmpty()

    @Synchronized
    fun getInt(key: String): Int = sharedPreferences.getInt(key, 0)

    @Synchronized
    fun getLong(key: String): Long = sharedPreferences.getLong(key, 0L)

    @Synchronized
    fun getFloat(key: String): Float = sharedPreferences.getFloat(key, 0F)

    @Synchronized
    fun getBoolean(key: String, defValue: Boolean = false): Boolean =
        sharedPreferences.getBoolean(key, defValue)

    @Synchronized
    fun isContain(key: String): Boolean = sharedPreferences.contains(key)

    @Synchronized
    fun remove(key: String) = sharedPreferences.edit().remove(key).commit()

    @Synchronized
    open fun clear() = sharedPreferences.edit().clear().commit()
}