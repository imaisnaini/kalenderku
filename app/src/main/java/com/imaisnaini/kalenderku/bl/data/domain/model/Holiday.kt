package com.imaisnaini.kalenderku.bl.data.domain.model
import java.time.LocalDate

data class Holiday(
    val holidayID: String,
    val name: String,
    val holidayDate: LocalDate,
    val commonField: CommonField? = null
)
