package com.imaisnaini.kalenderku.bl.data.remote.response.user

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.response.CommonFieldResponse

data class UserResponse(
    @SerializedName("user_id") val userID: String,
    @SerializedName("fullname") val fullname: String,
    @SerializedName("email") val email: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("password") val password: String? = null,
    @SerializedName("commonField") val commonFieldResponse: CommonFieldResponse? = null
) {
    fun mapToLocal() = User(
        userID = userID,
        fullname = fullname,
        email = email,
        phone = phone,
        password = password,
        commonField = commonFieldResponse?.mapToLocal()
    )
}
