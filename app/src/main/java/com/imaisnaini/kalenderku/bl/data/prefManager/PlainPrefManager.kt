package id.bni.maverick.core.util.prefManager

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.imaisnaini.kalenderku.bl.Constants.PREF_MANAGER

class PlainPrefManager(app: Context) : BasePrefManager() {
    override val sharedPreferences: SharedPreferences =
        app.getSharedPreferences(PREF_MANAGER, MODE_PRIVATE)
}