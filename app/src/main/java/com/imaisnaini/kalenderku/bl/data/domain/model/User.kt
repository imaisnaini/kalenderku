package com.imaisnaini.kalenderku.bl.data.domain.model

data class User(
    val userID: String,
    val fullname: String,
    val email: String,
    val phone: String,
    val password: String? = null,
    val commonField: CommonField? = null
)
