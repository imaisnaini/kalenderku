package com.imaisnaini.kalenderku.bl.data.remote.response.event

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.UserFriend
import com.imaisnaini.kalenderku.bl.data.remote.response.CommonFieldResponse
import java.time.ZoneId
import java.util.Date

data class EventResponse(
    @SerializedName("event_id") val eventID: String,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("event_date") val eventDate: Date,
    @SerializedName("color") val color: String,
    @SerializedName("status") val status: String,
    @SerializedName("is_notification") val isNotification: Boolean,
    @SerializedName("is_privacy") val isPrivacy: Boolean,
    @SerializedName("friends") val friendsResponse: List<UserFriendResponse>?,
    @SerializedName("commonField") val commonFieldResponse: CommonFieldResponse?
) {
    fun mapToLocal() = Event(
        eventID = eventID,
        name = name,
        description = description,
        eventDate = eventDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
        color = color,
        isNotification = isNotification,
        isPrivacy = isPrivacy,
        status = status,
        commonField = commonFieldResponse?.mapToLocal(),
        friends = friendsResponse?.map { it.mapToLocal() }
    )
}


