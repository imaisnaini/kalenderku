package com.imaisnaini.kalenderku.bl.data.remote.response

import com.google.gson.annotations.SerializedName

data class BaseDTO<A>(
    val data: A? = null,
    val error: String? = null,
    val success: Boolean? = true
)
