package com.imaisnaini.kalenderku.bl.data.domain.cache

import com.google.gson.Gson
import com.imaisnaini.kalenderku.bl.Constants.PreferencesConstants.PREF_KEY_LOGIN_TOKEN
import com.imaisnaini.kalenderku.bl.Constants.PreferencesConstants.PREF_KEY_LOGIN_USER
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.util.JsonUtil.fromJson
import com.imaisnaini.kalenderku.util.JsonUtil.toJson
import id.bni.maverick.core.util.prefManager.PlainPrefManager
import javax.inject.Inject

class KalenderKuCache @Inject constructor(
    private val plainPrefManager: PlainPrefManager
){

    fun loadToken() = plainPrefManager.getString(PREF_KEY_LOGIN_TOKEN).orEmpty()
    fun loadUser() = plainPrefManager.getString(PREF_KEY_LOGIN_USER)?.let { fromJson<User>(it) }
    fun saveLogin(token: String, user: User) {
        plainPrefManager.setString(PREF_KEY_LOGIN_TOKEN, token)
        plainPrefManager.setString(PREF_KEY_LOGIN_USER, toJson(user))
    }

    fun logOut() = plainPrefManager.clear()
}