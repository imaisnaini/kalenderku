package com.imaisnaini.kalenderku.bl.di

import android.content.Context
import com.imaisnaini.kalenderku.BuildConfig
import com.imaisnaini.kalenderku.bl.Constants.APIURL.BASE_URL
import com.imaisnaini.kalenderku.bl.Constants.PreferencesConstants.PREF_KEY_LOGIN_TOKEN
import com.imaisnaini.kalenderku.bl.api.ApiService
import com.imaisnaini.kalenderku.bl.data.domain.cache.KalenderKuCache
import com.imaisnaini.kalenderku.bl.data.remote.repository.Repository
import com.imaisnaini.kalenderku.bl.data.remote.service.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.bni.maverick.core.util.prefManager.PlainPrefManager
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule{

    @Provides
    fun provideBaseUrl() = BASE_URL

    @Singleton
    @Provides
    fun provideHttpClient(plainPrefManager: PlainPrefManager): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        val interceptor: Interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {
                val originalRequest = chain.request()

                val newRequest = originalRequest.newBuilder().apply {
                    headers(Headers.Builder().apply {
                        addAll(originalRequest.headers)
                        val token = plainPrefManager.getString(PREF_KEY_LOGIN_TOKEN)
                        if (token != null) {
                            add("Authorization", token)
                            if (BuildConfig.DEBUG) {
                                Timber.i("Authorization", "token : $token")
                            }
                        }
                    }.build())
                }.build()
                return chain.proceed(newRequest)
            }
        }
        return OkHttpClient
            .Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(interceptor)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()
    }
    @Singleton
    @Provides
    fun provideConverterFactory(): GsonConverterFactory =
        GsonConverterFactory.create()

    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(gsonConverterFactory)
            .build()
    }
    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: ApiService) = RemoteDataSource(apiService)

    @Provides
    @Singleton
    fun provideKalenderKuCache(plainPrefManager: PlainPrefManager) =
        KalenderKuCache(plainPrefManager)

    @Provides
    @Singleton
    fun proviedRepository(remote: RemoteDataSource, cache: KalenderKuCache) = Repository(remote, cache)

    @Singleton
    @Provides
    fun providePlainPrefManager(@ApplicationContext context: Context) = PlainPrefManager(context)
}
