package com.imaisnaini.kalenderku.bl.data.remote.response.friend

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.Friend

data class GetFriendByUserIDResponse(
    @SerializedName("fullname") val fullname: String,
    @SerializedName("email") val email: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("friend_id")
    val friendID: String,
    @SerializedName("user_id")
    val userID: String,
    @SerializedName("friend_user_id")
    val friendUserID: String,
    @SerializedName("status")
    val status: String,
) {
    fun mapToLocal() = Friend(
        friendID = friendID,
        userID = userID,
        friendUserID = friendUserID,
        fullname = fullname,
        email = email,
        phone = phone,
        status = status,
    )
}

