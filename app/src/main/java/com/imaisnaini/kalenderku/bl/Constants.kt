package com.imaisnaini.kalenderku.bl

object Constants {
    const val PREF_MANAGER = "prefsKalenderKu"
    const val AUTHORIZATION = "Authorization"
    object APIURL {
        const val BASE_URL = "http://192.168.30.204:1323/api/"
        const val EVENT = "v1/event"
        const val FRIEND = "v1/friend"
        const val USER = "v1/user"
        const val HOLIDAY = "v1/holiday"
    }

    object EventStatus {
        const val PENDING = "pending"
        const val ACCEPT = "accept"
        const val REJECT = "reject"
        const val OWNER = "owner"
    }

    object HttpErrorCode {
        const val NO_CONTENT = 204
        const val BAD_REQUEST = 400
        const val INVALID_TOKEN = 401
        const val API_NOT_FOUND = 404
        const val CONFLICT = 409
        const val APP_TIME_OUT = 500
        const val SERVICE_UNAVAILABLE = 503
        const val GATEWAY_TIME_OUT = 504
    }

    object PreferencesConstants {
        const val PREF_KEY_LOGIN_TOKEN = "token"
        const val PREF_KEY_LOGIN_USER = "userActive"
    }
}