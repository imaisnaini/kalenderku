package com.imaisnaini.kalenderku.bl.data.domain.model

data class UserEvent(
    val userEventID: String,
    val userID: String,
    val eventID: String,
    val status: String,
    val commonField: CommonField? = null
)
