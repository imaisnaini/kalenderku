package com.imaisnaini.kalenderku.bl.data.domain.model

import java.time.LocalDateTime

data class CommonField(
    val createdAt: LocalDateTime? = null,
    val createdBy: Long? = null,
    val updatedAt: LocalDateTime? = null,
    val updatedBy: Long? = null

)
