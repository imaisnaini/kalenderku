package com.imaisnaini.kalenderku.bl.api

import com.imaisnaini.kalenderku.bl.Constants.APIURL.EVENT
import com.imaisnaini.kalenderku.bl.Constants.APIURL.FRIEND
import com.imaisnaini.kalenderku.bl.Constants.APIURL.HOLIDAY
import com.imaisnaini.kalenderku.bl.Constants.APIURL.USER
import com.imaisnaini.kalenderku.bl.data.remote.request.event.CreateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.DeleteEventReqeust
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateStatusUserEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.CreateFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.DeleteFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.UpdateStatusFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.CreateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.LoginRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.UpdateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.event.CreateEventResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.event.EventResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.friend.CreateFriendResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.friend.GetFriendByUserIDResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.holiday.HolidayResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.LoginResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.UserResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Query

interface ApiService {
    @POST("$USER/register")
    suspend fun createUser(@Body request: CreateUserRequest): Response<BaseDTO<UserResponse>>

    @POST("$USER/login")
    suspend fun login(@Body request: LoginRequest): Response<BaseDTO<LoginResponse>>

    @PUT(USER)
    suspend fun updateUser(@Body request: UpdateUserRequest): Response<BaseDTO<Any>>
    
    @GET(USER)
    suspend fun getUserByPhone(@Query("phone") phone: String): Response<BaseDTO<List<UserResponse>>>

    @GET(EVENT)
    suspend fun getEvent(
        @Query("user_id") userID: String,
        @Query("event_date") eventDate: String
    ): Response<BaseDTO<List<EventResponse>>>

    @GET("$EVENT/detail")
    suspend fun getEventDetail(
        @Query("event_id") eventID: String
    ): Response<BaseDTO<EventResponse>>

    @PUT(EVENT)
    suspend fun updteEvent(@Body request: UpdateEventRequest): Response<BaseDTO<Any>>

    @POST(EVENT)
    suspend fun createEvent(@Body request: CreateEventRequest): Response<BaseDTO<CreateEventResponse>>

    @HTTP(method = "DELETE", path = EVENT, hasBody = true)
    suspend fun deleteEvent(@Body reqeust: DeleteEventReqeust): Response<BaseDTO<Any>>

    @PATCH("$EVENT/status")
    suspend fun patchEvent(@Body request: UpdateStatusUserEventRequest): Response<BaseDTO<Any>>

    @GET(FRIEND)
    suspend fun getFriend(@Query("status") status: String): Response<BaseDTO<List<GetFriendByUserIDResponse>>>

    @POST(FRIEND)
    suspend fun createFriend(@Body request: CreateFriendRequest): Response<BaseDTO<CreateFriendResponse>>

    @PATCH("$FRIEND/status")
    suspend fun patchFriend(@Body request: UpdateStatusFriendRequest): Response<BaseDTO<Any>>

    @HTTP(method = "DELETE", path = FRIEND, hasBody = true)
    suspend fun deleteFriend(@Body request: DeleteFriendRequest): Response<BaseDTO<Any>>

    @GET(HOLIDAY)
    suspend fun getHoliday(): Response<BaseDTO<List<HolidayResponse>>>
}