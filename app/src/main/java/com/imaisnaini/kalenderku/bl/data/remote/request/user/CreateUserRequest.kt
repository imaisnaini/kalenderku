package com.imaisnaini.kalenderku.bl.data.remote.request.user

import com.google.gson.annotations.SerializedName

data class CreateUserRequest(
    @SerializedName("fullname") val fullname: String,
    @SerializedName("email") val email: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("password") val password: String
)
