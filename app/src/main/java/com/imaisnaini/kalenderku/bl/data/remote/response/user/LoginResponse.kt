package com.imaisnaini.kalenderku.bl.data.remote.response.user

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

data class LoginResponse(
    @SerializedName("email") val email: String,
    @SerializedName("fullname") val fullname: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("user_id") val userId: String,
    @SerializedName("token") val token: String,
) {
    fun mapToLocal() = User(
        userID = userId,
        fullname = fullname,
        email = email,
        phone = phone,
        password = EMPTY_STRING
    )
}
