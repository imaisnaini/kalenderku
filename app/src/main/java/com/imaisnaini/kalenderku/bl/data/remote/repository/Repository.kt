package com.imaisnaini.kalenderku.bl.data.remote.repository

import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.data.domain.cache.KalenderKuCache
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.request.event.CreateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.DeleteEventReqeust
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateStatusUserEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.CreateFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.DeleteFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.UpdateStatusFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.CreateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.LoginRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.UpdateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.event.CreateEventResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.event.EventResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.friend.CreateFriendResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.friend.GetFriendByUserIDResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.holiday.HolidayResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.LoginResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.UserResponse
import com.imaisnaini.kalenderku.bl.data.remote.service.BaseDataSource
import com.imaisnaini.kalenderku.bl.data.remote.service.RemoteDataSource
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

@ActivityRetainedScoped
class Repository @Inject constructor(
    private val remote: RemoteDataSource,
    private val cache: KalenderKuCache
): BaseDataSource() {
    suspend fun login(request: LoginRequest): Flow<ApiResult<BaseDTO<LoginResponse>>> {
        return flow {
            emit(getResultWithSingleObject { remote.login(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createUser(request: CreateUserRequest): Flow<ApiResult<BaseDTO<UserResponse>>> {
        return flow {
            emit(getResultWithSingleObject { remote.createUser(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun updateUser(request: UpdateUserRequest): Flow<ApiResult<BaseDTO<Any>>> {
        return flow {
            emit(getResultWithSingleObject { remote.updateUser(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getUserByPhone(phone: String): Flow<ApiResult<BaseDTO<List<UserResponse>>>> {
        return flow {
            emit(getResultWithSingleObject { remote.getUserByPhone(phone) })
        }.flowOn(Dispatchers.IO)
    }
    suspend fun createEvent(request: CreateEventRequest): Flow<ApiResult<BaseDTO<CreateEventResponse>>> {
        return flow {
            emit(getResultWithSingleObject { remote.createEvent(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun createFriend(request: CreateFriendRequest): Flow<ApiResult<BaseDTO<CreateFriendResponse>>> {
        return flow {
            emit(getResultWithSingleObject { remote.createFriend(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getEvent(
        userID: String,
        eventDate: String
    ): Flow<ApiResult<BaseDTO<List<EventResponse>>>> {
        return flow {
            emit(getResultWithSingleObject { remote.
            getEvent(userID, eventDate) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getEventDetail(
        eventID: String
    ): Flow<ApiResult<BaseDTO<EventResponse>>> {
        return flow {
            emit(getResultWithSingleObject { remote.
            getEventDetail(eventID) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getFriend(status: String): Flow<ApiResult<BaseDTO<List<GetFriendByUserIDResponse>>>> {
        return flow {
            emit(getResultWithSingleObject { remote.getFriend(status) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getHoliday(): Flow<ApiResult<BaseDTO<List<HolidayResponse>>>> {
        return flow {
            emit(getResultWithSingleObject { remote.getHoliday() })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun updateEvent(request: UpdateEventRequest): Flow<ApiResult<BaseDTO<Any>>> {
        return flow {
            emit(getResultWithSingleObject { remote.updateEvent(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun updateStatusEvent(request: UpdateStatusUserEventRequest): Flow<ApiResult<BaseDTO<Any>>> {
        return flow {
            emit(getResultWithSingleObject { remote.updateStatusEvent(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun updateStatusFriend(request: UpdateStatusFriendRequest): Flow<ApiResult<BaseDTO<Any>>> {
        return flow {
            emit(getResultWithSingleObject { remote.updateStatusFriend(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deleteEvent(request: DeleteEventReqeust): Flow<ApiResult<BaseDTO<Any>>> {
        return flow {
            emit(getResultWithSingleObject { remote.deleteEvent(request) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun deleteFriend(request: DeleteFriendRequest): Flow<ApiResult<BaseDTO<Any>>> {
        return flow {
            emit(getResultWithSingleObject { remote.deleteFriend(request) })
        }.flowOn(Dispatchers.IO)
    }


    fun loadToken() = cache.loadToken()

    fun loadUser() = cache.loadUser()

    fun saveLogin(token: String, user: User) = cache.saveLogin(token, user)

    fun logOut() = cache.logOut()
}