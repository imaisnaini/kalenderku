package com.imaisnaini.kalenderku.bl.data.remote.request.user

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

data class UpdateUserRequest(
    @SerializedName("confirm_password")
    val confirmPassword: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("new_password")
    val newPassword: String,
    @SerializedName("phone")
    val phone: String
) {
    fun mapToLocal(userID: String = EMPTY_STRING) = User(
        email = email,
        fullname = fullname,
        phone = phone,
        password = newPassword,
        userID = userID
    )
}