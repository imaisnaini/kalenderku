package com.imaisnaini.kalenderku.bl.data.domain.model

import com.imaisnaini.kalenderku.bl.Constants.EventStatus.ACCEPT
import java.time.LocalDate

data class Event(
    val eventID: String,
    val name: String,
    val eventDate: LocalDate,
    val color: String,
    val isNotification: Boolean,
    val isPrivacy: Boolean,
    val status: String = ACCEPT,
    val description: String? = null,
    val commonField: CommonField? = null,
    val friends: List<UserFriend>? = emptyList()
)
