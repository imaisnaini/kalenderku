package com.imaisnaini.kalenderku.bl.data.remote.request.user

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("phone") val phone: String,
    @SerializedName("password") val password: String
)
