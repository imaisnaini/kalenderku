package com.imaisnaini.kalenderku.bl.data.remote.response.friend

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.remote.response.CommonFieldResponse

data class FriendResponse(
    @SerializedName("friend_id")
    val friendID: String,
    @SerializedName("user_id")
    val userID: String,
    @SerializedName("friend_user_id")
    val friendUserID: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("commonField")
    val commonFieldResponse: CommonFieldResponse
)
