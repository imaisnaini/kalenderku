package com.imaisnaini.kalenderku.bl.data.remote.response.event

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.Event

data class CreateEventResponse(
    @SerializedName("data")
    val data: Event
)
