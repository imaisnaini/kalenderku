package com.imaisnaini.kalenderku.bl.data.remote.request.friend

import com.google.gson.annotations.SerializedName
import java.util.Date

data class UpdateStatusFriendRequest(
    @SerializedName("friend_user_id")
    val friendUserID: String,
    @SerializedName("status")
    val status: String
)
