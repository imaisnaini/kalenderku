package com.imaisnaini.kalenderku.bl.data.remote.request.event

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

data class CreateEventRequest(
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String = EMPTY_STRING,
    @SerializedName("event_date")
    val eventDate: String,
    @SerializedName("color")
    val color: String,
    @SerializedName("is_notification")
    val isNotification: Boolean = true,
    @SerializedName("is_privacy")
    val isPrivacy: Boolean = true,
    @SerializedName("friend_ids")
    val friendIDs: List<String> = emptyList()
)
