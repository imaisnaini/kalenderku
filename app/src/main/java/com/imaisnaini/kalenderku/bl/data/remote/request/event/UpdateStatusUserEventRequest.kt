package com.imaisnaini.kalenderku.bl.data.remote.request.event

import com.google.gson.annotations.SerializedName
import java.util.Date

data class UpdateStatusUserEventRequest(
    @SerializedName("event_id")
    val eventID: String,
    @SerializedName("status")
    val status: String
)
