package com.imaisnaini.kalenderku.bl.data.remote.request.event

import com.google.gson.annotations.SerializedName
import java.util.Date

data class UpdateEventRequest(
    @SerializedName("event_id")
    val eventID: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("event_date")
    val eventDate: String,
    @SerializedName("color")
    val color: String,
    @SerializedName("is_notification")
    val isNotification: Boolean,
    @SerializedName("is_privacy")
    val isPrivacy: Boolean
)
