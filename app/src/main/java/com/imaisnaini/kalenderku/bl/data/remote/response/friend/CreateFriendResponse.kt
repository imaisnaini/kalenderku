package com.imaisnaini.kalenderku.bl.data.remote.response.friend

import com.google.gson.annotations.SerializedName

data class CreateFriendResponse(
    @SerializedName("data")
    val data: FriendResponse
)
