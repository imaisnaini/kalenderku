package com.imaisnaini.kalenderku.bl.data.remote.response.holiday

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.CommonField
import com.imaisnaini.kalenderku.bl.data.domain.model.Holiday
import com.imaisnaini.kalenderku.bl.data.remote.response.CommonFieldResponse
import kotlinx.datetime.LocalDateTime
import java.time.ZoneId
import java.util.Date

data class HolidayResponse(
    @SerializedName("holiday_id")
    val holidayID: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("holiday_date")
    val holidayDate: Date,
    @SerializedName("commonField")
    val commonFieldResponse: CommonFieldResponse? = null
) {
    fun mapToLocal() = Holiday(
        holidayID = holidayID,
        name = name,
        holidayDate = holidayDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
        commonField = commonFieldResponse?.mapToLocal()
    )
}
