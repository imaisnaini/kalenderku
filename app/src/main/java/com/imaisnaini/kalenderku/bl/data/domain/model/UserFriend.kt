package com.imaisnaini.kalenderku.bl.data.domain.model

import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

data class UserFriend(
    val fullName: String,
    val userID: String
) {
    fun mapToFriend() = Friend(
        friendID = userID,
        fullname = fullName,
        friendUserID = EMPTY_STRING,
        userID = EMPTY_STRING,
        email = EMPTY_STRING,
        phone = EMPTY_STRING,
        status = EMPTY_STRING
    )
}
