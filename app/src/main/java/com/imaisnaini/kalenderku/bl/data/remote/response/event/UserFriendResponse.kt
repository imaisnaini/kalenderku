package com.imaisnaini.kalenderku.bl.data.remote.response.event

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.UserFriend

data class UserFriendResponse(
    @SerializedName("fullname") val fullName: String,
    @SerializedName("user_id") val userID: String
) {
    fun mapToLocal() = UserFriend(fullName, userID)
}