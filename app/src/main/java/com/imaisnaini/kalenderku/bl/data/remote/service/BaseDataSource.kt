package com.imaisnaini.kalenderku.bl.data.remote.service

import com.imaisnaini.kalenderku.bl.Constants.HttpErrorCode.GATEWAY_TIME_OUT
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.util.Constants.SPACE_STRING
import com.imaisnaini.kalenderku.util.Constants.ZERO
import okhttp3.ResponseBody
import retrofit2.Response
import timber.log.Timber
import java.net.SocketTimeoutException

abstract class BaseDataSource {
    suspend fun <T> getResultWithSingleObject(call: suspend () -> Response<T>): ApiResult<T> = try {
        val response = call()
        if (response.isSuccessful) Success(response.body())
        else errorHandler(response.code(), response.errorBody())
    } catch (ex: SocketTimeoutException) {
        errorHandler(GATEWAY_TIME_OUT, null)
    } catch (e: Exception) {
        Timber.e(e)
        errorHandler(ZERO, null)
    }

    private fun <T> errorHandler(httpCode: Int, errorBody: ResponseBody?): ApiResult<T> {
        return ApiResult.Error(
            error = "$httpCode$SPACE_STRING$errorBody"
        )
    }
}