package com.imaisnaini.kalenderku.bl.data.remote.response

import com.google.gson.annotations.SerializedName
import com.imaisnaini.kalenderku.bl.data.domain.model.CommonField
import kotlinx.datetime.LocalDateTime
import java.time.ZoneId
import java.util.Date

data class CommonFieldResponse(
    @SerializedName("created_at") val createdAt: Date? = null,
    @SerializedName("created_by") val createdBy: Long? = null,
    @SerializedName("updated_at") val updatedAt: Date? = null,
    @SerializedName("updated_by") val updatedBy: Long? = null
) {
    fun mapToLocal() = CommonField(
        createdAt = createdAt?.toInstant()?.atZone(ZoneId.systemDefault())?.toLocalDateTime(),
        updatedAt = updatedAt?.toInstant()?.atZone(ZoneId.systemDefault())?.toLocalDateTime(),
        createdBy = createdBy,
        updatedBy = updatedBy
    )
}
