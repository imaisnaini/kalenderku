package com.imaisnaini.kalenderku.bl.data.remote.request.friend

import com.google.gson.annotations.SerializedName

data class CreateFriendRequest(
    @SerializedName("friend_user_id")
    val friendUserID: String
)
