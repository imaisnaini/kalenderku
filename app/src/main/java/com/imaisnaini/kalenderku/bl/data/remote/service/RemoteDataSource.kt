package com.imaisnaini.kalenderku.bl.data.remote.service

import com.imaisnaini.kalenderku.bl.api.ApiService
import com.imaisnaini.kalenderku.bl.data.remote.request.event.CreateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.DeleteEventReqeust
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateStatusUserEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.CreateFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.DeleteFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.UpdateStatusFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.CreateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.LoginRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.UpdateUserRequest
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val apiService: ApiService
) {
    suspend fun login(request: LoginRequest) = apiService.login(request)

    suspend fun createUser(request: CreateUserRequest) = apiService.createUser(request)

    suspend fun updateUser(request: UpdateUserRequest) = apiService.updateUser(request)

    suspend fun getUserByPhone(phone: String) = apiService.getUserByPhone(phone)

    suspend fun createEvent(request: CreateEventRequest) = apiService.createEvent(request)

    suspend fun createFriend(request: CreateFriendRequest) = apiService.createFriend(request)

    suspend fun getEvent(
        userID: String,
        eventDate: String
    ) = apiService.getEvent(userID, eventDate)

    suspend fun getEventDetail(
        eventID: String
    ) = apiService.getEventDetail(eventID)

    suspend fun getFriend(status: String) = apiService.getFriend(status)

    suspend fun getHoliday() = apiService.getHoliday()

    suspend fun updateEvent(request: UpdateEventRequest) = apiService.updteEvent(request)

    suspend fun updateStatusEvent(request: UpdateStatusUserEventRequest) = apiService.patchEvent(request)

    suspend fun updateStatusFriend(request: UpdateStatusFriendRequest) = apiService.patchFriend(request)

    suspend fun deleteEvent(request: DeleteEventReqeust) = apiService.deleteEvent(request)

    suspend fun deleteFriend(request: DeleteFriendRequest) = apiService.deleteFriend(request)
}