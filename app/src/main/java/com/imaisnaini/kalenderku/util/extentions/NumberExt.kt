package com.imaisnaini.kalenderku.util.extentions

import com.imaisnaini.kalenderku.util.Constants.ELEVEN
import com.imaisnaini.kalenderku.util.Constants.ONE
import com.imaisnaini.kalenderku.util.Constants.ONE_HUNDRED
import com.imaisnaini.kalenderku.util.Constants.ORDINAL_ND
import com.imaisnaini.kalenderku.util.Constants.ORDINAL_RD
import com.imaisnaini.kalenderku.util.Constants.ORDINAL_ST
import com.imaisnaini.kalenderku.util.Constants.ORDINAL_TH
import com.imaisnaini.kalenderku.util.Constants.TEN
import com.imaisnaini.kalenderku.util.Constants.THIRTEEN
import com.imaisnaini.kalenderku.util.Constants.THREE
import com.imaisnaini.kalenderku.util.Constants.TWO
import com.imaisnaini.kalenderku.util.Constants.ZERO
import com.imaisnaini.kalenderku.util.Constants.ZERO_DOUBLE
import com.imaisnaini.kalenderku.util.Constants.ZERO_FLOAT
import com.imaisnaini.kalenderku.util.Constants.ZERO_LONG
import java.math.BigDecimal

fun Int?.orZero(): Int = this ?: ZERO

fun Double?.orZero(): Double = this ?: ZERO_DOUBLE

fun Long?.orZero(): Long = this ?: ZERO_LONG

fun Float?.orZero(): Float = this ?: ZERO_FLOAT

fun BigDecimal?.orZero(): BigDecimal = this ?: BigDecimal.ZERO

fun BigDecimal?.isZero() =
    this?.let { it == BigDecimal.ZERO || it == ZERO_FLOAT.toBigDecimal() } ?: true

fun BigDecimal?.isMoreThanZero() =
    this?.let { it > BigDecimal.ZERO || it > ZERO_FLOAT.toBigDecimal() } ?: false

fun Iterable<BigDecimal>.sum(): BigDecimal {
    var sum: BigDecimal = BigDecimal.ZERO
    this.forEach { element -> sum += element }
    return sum
}

fun Int?.orReplaceWith(replacement: Int) = this ?: replacement

fun Int.toOrdinal(): String = when {
    this % ONE_HUNDRED in ELEVEN..THIRTEEN -> "${this}$ORDINAL_TH"
    this % TEN == ONE -> "${this}$ORDINAL_ST"
    this % TEN == TWO -> "${this}$ORDINAL_ND"
    this % TEN == THREE -> "${this}$ORDINAL_RD"
    else -> "${this}$ORDINAL_TH"
}