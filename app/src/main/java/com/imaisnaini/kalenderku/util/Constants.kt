package com.imaisnaini.kalenderku.util

object Constants {
    const val MINUS_ONE = -1
    const val ZERO = 0
    const val ZERO_STRING = "0"
    const val ZERO_DOUBLE = 0.0
    const val ZERO_LONG = 0L
    const val ONE_LONG = 1L
    const val THREE_LONG = 3L
    const val SEVEN_LONG = 7L
    const val THIRTY_LONG = 30L
    const val THIRTY_ONE_LONG = 31L
    const val ONE_THOUSAND_LONG = 1000L
    const val FIVE_HUNDRED_LONG = 500L
    const val QUARTER_FLOAT = 0.25F
    const val ZERO_FLOAT = 0F
    const val ONE_FLOAT = 1F
    const val TWO_FLOAT = 2F
    const val THREE_FLOAT = 3F
    const val FOUR_FLOAT = 4F
    const val FIVE_FLOAT = 5F
    const val SIX_FLOAT = 6F
    const val NINE_FLOAT = 9F
    const val TEN_FLOAT = 10F
    const val THIRTY_FLOAT = 30F
    const val SEVENTY_FLOAT = 70F
    const val ONE_HUNDRED_FLOAT = 100F
    const val TWO_HUNDRED_SEVENTY_FLOAT = 270F
    const val THREE_HUNDRED_SIXTY_FLOAT = 360F
    const val ONE = 1
    const val TWO = 2
    const val THREE = 3
    const val FOUR = 4
    const val FIVE = 5
    const val SIX = 6
    const val SEVEN = 7
    const val EIGHT = 8
    const val NINE = 9
    const val TEN = 10
    const val ELEVEN = 11
    const val TWELVE = 12
    const val THIRTEEN = 13
    const val FOURTEEN = 14
    const val FIFTEEN = 15
    const val SIX_TEEN = 16
    const val TWENTY = 20
    const val TWENTY_ONE = 21
    const val TWENTY_FOUR = 24
    const val TWENTY_FIVE = 25
    const val THIRTY = 30
    const val THIRTY_ONE = 31
    const val THIRTY_TWO = 32
    const val FORTY_EIGHT = 48
    const val FIFTY = 50
    const val FIFTY_TWO = 52
    const val SIXTY = 60
    const val SIXTY_ONE = 61
    const val SIXTY_FOUR = 64
    const val EIGHTY = 80
    const val EIGHTY_ONE = 81
    const val NINETY = 90
    const val NINETY_NINE = 99
    const val ONE_HUNDRED = 100
    const val TWO_HUNDRED = 200
    const val FOUR_HUNDRED = 400
    const val ONE_THOUSAND = 1000
    const val SPACE_STRING = " "
    const val EMPTY_STRING = ""
    const val DOT = "."
    const val BULLET = "•"
    const val COMMA = ","
    const val DEFAULT_ELLIPSES_LIMIT = 25
    const val ELLIPSES_SIGN = "..."
    const val QUESTION = "?"
    const val AMPERSAND = "&"
    const val PERCENT = "%"
    const val REGEX_SQUARE_BRACKET_START = "["
    const val REGEX_SQUARE_BRACKET_END = "]"
    const val STRING_NULL = "null"
    const val AT_EMAIL = "@"
    const val ASTERISK_MASK = "*"
    const val SPACE_CHAR = ' '
    const val MASKED_ACC_NUMBER_STRING = "•"
    val RANGE_OF_MASK_PHONE_NUMBER = ZERO..TWO
    const val PIPE = '|'
    const val HYPHEN_WITH_SPACE = " - "
    const val HYPHEN = "-"
    const val PLUS = "+"
    const val SLASH = "/"
    const val SEMI_COLON = ";"
    const val COLON = ":"
    const val NEW_LINE = "\n"
    const val REFRESH_TOKEN_DELAY = 60
    const val EQUALS = "=="
    const val UNDERSCORE = "_"
    const val DEFAULT_STATE_TIMEOUT: Long = 5000
    const val OKHTTP_CACHE_MEGABYTE = 50
    const val BYTE_MULTIPLIER = 1024
    const val BIT_MULTIPLIER = 1024
    const val MAX_AGE_CACHE = 60000
    const val DELAY_COROUTINE_SEQUENTIAL = 200L
    const val INDONESIA_COUNTRY_CODE = "62"

    const val ORDINAL_ST = "st"
    const val ORDINAL_ND = "nd"
    const val ORDINAL_RD = "rd"
    const val ORDINAL_TH = "th"

    const val PACKAGE = "package"
    const val UTF_8 = "UTF-8"
    const val CONTENT = "__CONTENT__"

    const val CHAR_A = 'A'
    const val CHAR_Z = 'Z'
    const val CHAR_ZERO = '0'
    const val CHAR_NINE = '9'

    const val BOLD_TAG = "<b>$CONTENT</b>"

    object Language {
        const val LANGUAGE_EN_ID = "en-ID"
        const val LANGUAGE_ID_ID = "id-ID"
        const val LANGUAGE_IN_ID = "in-ID"

        const val LOCALE_LANGUAGE_ID = "in"
        const val LOCALE_LANGUAGE_EN = "en"
        const val LOCALE_COUNTRY_ID = "ID"
    }

    object FriendStatus {
        const val ACCEPT = "accept"
        const val PENDING = "pending"
    }
}
