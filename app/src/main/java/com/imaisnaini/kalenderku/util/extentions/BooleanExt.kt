package com.imaisnaini.kalenderku.util.extentions

fun Boolean?.orTrue(): Boolean = this ?: true
fun Boolean?.orFalse(): Boolean = this ?: false

inline fun Boolean.ifTrueAndReturn(block: () -> Unit): Boolean {
    if (this) block()
    return this
}

inline fun Boolean.ifFalseAndReturn(block: () -> Unit): Boolean {
    if (!this) block()
    return this
}
