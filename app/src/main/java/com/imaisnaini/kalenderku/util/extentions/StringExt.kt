package com.imaisnaini.kalenderku.util.extentions

import android.graphics.Typeface
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.core.text.HtmlCompat
import com.imaisnaini.kalenderku.util.Constants
import com.imaisnaini.kalenderku.util.Constants.ASTERISK_MASK
import com.imaisnaini.kalenderku.util.Constants.BULLET
import com.imaisnaini.kalenderku.util.Constants.COMMA
import com.imaisnaini.kalenderku.util.Constants.CONTENT
import com.imaisnaini.kalenderku.util.Constants.DEFAULT_ELLIPSES_LIMIT
import com.imaisnaini.kalenderku.util.Constants.DOT
import com.imaisnaini.kalenderku.util.Constants.EIGHT
import com.imaisnaini.kalenderku.util.Constants.ELEVEN
import com.imaisnaini.kalenderku.util.Constants.ELLIPSES_SIGN
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Constants.FIFTEEN
import com.imaisnaini.kalenderku.util.Constants.FIVE
import com.imaisnaini.kalenderku.util.Constants.FOUR
import com.imaisnaini.kalenderku.util.Constants.HYPHEN
import com.imaisnaini.kalenderku.util.Constants.HYPHEN_WITH_SPACE
import com.imaisnaini.kalenderku.util.Constants.MINUS_ONE
import com.imaisnaini.kalenderku.util.Constants.ONE
import com.imaisnaini.kalenderku.util.Constants.REGEX_SQUARE_BRACKET_END
import com.imaisnaini.kalenderku.util.Constants.REGEX_SQUARE_BRACKET_START
import com.imaisnaini.kalenderku.util.Constants.SIX_TEEN
import com.imaisnaini.kalenderku.util.Constants.SPACE_STRING
import com.imaisnaini.kalenderku.util.Constants.STRING_NULL
import com.imaisnaini.kalenderku.util.Constants.THREE
import com.imaisnaini.kalenderku.util.Constants.TWO
import com.imaisnaini.kalenderku.util.Constants.ZERO
import java.util.regex.PatternSyntaxException

fun String.trimWordsWithEllipses(limit: Int = DEFAULT_ELLIPSES_LIMIT) =
    if (length > limit) subSequence(ZERO, limit).toString().plus(ELLIPSES_SIGN)
    else this

fun String?.orReplaceWith(replacement: String) = this ?: replacement

fun String.orReplaceWhen(condition: Boolean, replacement: String) =
    if (condition) replacement else this

fun String.orNull() = this.ifBlank { null }

fun String?.toRegexOrNull(): Regex? = try {
    this?.lowercase()?.toRegex()
} catch (ex: PatternSyntaxException) {
    null
}

fun String?.takeValidString(): String =
    if (this.isNullOrBlank() || this.equals(STRING_NULL, ignoreCase = true)) EMPTY_STRING
    else this

fun String.fromHtml() = HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)

fun String.clearSquareBrackets() =
    this.replace(REGEX_SQUARE_BRACKET_START, EMPTY_STRING)
        .replace(REGEX_SQUARE_BRACKET_END, EMPTY_STRING)

fun String.putInParentheses() = "($this)"

fun String.surroundWithSpace() = " $this "

fun CharSequence?.removeNonDigit() =
    this?.filter { it.isDigit() }.orDefault(EMPTY_STRING).toString()

fun String?.joinWithHyphen(rightText: String) =
    this?.trimEnd().plus(HYPHEN_WITH_SPACE).plus(rightText.trimEnd())
        .removeSuffix(HYPHEN_WITH_SPACE)
        .removePrefix(HYPHEN_WITH_SPACE)

fun String?.appendDot() = if (this.isNullOrBlank()) EMPTY_STRING else "$this$DOT"

fun String?.appendHyphen() = if (this.isNullOrBlank()) EMPTY_STRING else "$this$HYPHEN"

fun String?.appendComma() = if (this.isNullOrBlank()) EMPTY_STRING else "$this$COMMA$SPACE_STRING"

fun String.getInitials(
    uppercase: Boolean = true,
    separator: String = SPACE_STRING
): String {
    val splitWords = this.trim().split(separator)
    return splitWords.firstOrNull().orEmpty().first().upperLowerCase(uppercase) +
            if (splitWords.size > ONE) splitWords.lastOrNull().orEmpty().first()
                .upperLowerCase(uppercase) else EMPTY_STRING
}

fun String.isRepeated(): Boolean {
    if (length <= ONE) return false
    var result = true
    for (i in ONE until length) {
        if (this[i] != this[ZERO]) {
            result = false
            break
        }
    }
    return result
}

fun String.isAscending(): Boolean {
    if (length <= ONE) return false
    var isAscending = true
    for (i in ONE until length) {
        val diff = this[i] - this[i - ONE]
        if (diff <= ZERO || diff > ONE) {
            isAscending = false
            break
        }
    }
    return isAscending
}

fun String.isDescending(): Boolean {
    if (length <= ONE) return false
    var isDescending = true
    for (i in ONE until length) {
        val diff = this[i] - this[i - ONE]
        if (diff >= ZERO || diff < MINUS_ONE) {
            isDescending = false
            break
        }
    }
    return isDescending
}

fun String.isAscendingOrDescending() = this.isAscending() || this.isDescending()

fun metadataText(value: List<String>) = value.take(FOUR).joinToString(" $BULLET ")

fun Char.upperLowerCase(uppercase: Boolean) = if (uppercase) uppercase() else lowercase()

fun String.isIdentical(comparedString: String): Boolean {
    return this == comparedString
}

fun String?.isOneWord() =
    if (!this.isNullOrBlank()) trim().split(SPACE_STRING).size == ONE
    else false

fun String.capitalizeEachWord() = lowercase().split(SPACE_STRING)
    .joinToString(SPACE_STRING) { word -> word.replaceFirstChar { char -> char.uppercaseChar() } }

fun String.wrapWithTag(tag: String) = tag.replace(CONTENT, this)

fun String.trimAndPad(char: Char, length: Int) = trimStart(char).take(length).padStart(length, char)

fun Spanned.toAnnotatedString(): AnnotatedString = buildAnnotatedString {
    val spanned = this@toAnnotatedString
    append(spanned.toString())
    getSpans(ZERO, spanned.length, Any::class.java).forEach { span ->
        val start = getSpanStart(span)
        val end = getSpanEnd(span)
        when (span) {
            is StyleSpan -> when (span.style) {
                Typeface.BOLD -> addStyle(SpanStyle(fontWeight = FontWeight.Bold), start, end)
                Typeface.ITALIC -> addStyle(SpanStyle(fontStyle = FontStyle.Italic), start, end)
                Typeface.BOLD_ITALIC -> addStyle(
                    SpanStyle(fontWeight = FontWeight.Bold, fontStyle = FontStyle.Italic),
                    start,
                    end
                )
            }

            is UnderlineSpan -> addStyle(SpanStyle(textDecoration = TextDecoration.Underline), start, end)
            is ForegroundColorSpan -> addStyle(
                SpanStyle(color = Color(span.foregroundColor)),
                start,
                end
            )
        }
    }
}

fun String?.ifNullOrBlank(newValue: () -> String) = if (isNullOrBlank()) newValue() else this

fun String.replacePrefixPhoneNumber() =
    this.replace(Constants.PLUS + Constants.INDONESIA_COUNTRY_CODE, Constants.ZERO_STRING).filter { it.isDigit() }

fun String.maskPhoneNumber(): String {
    var maskedPhoneNumber = EMPTY_STRING

    this.forEachIndexed { i, number ->
        maskedPhoneNumber += if (i in TWO..this.length - FIVE) ASTERISK_MASK else number
    }

    return maskedPhoneNumber
}