package com.imaisnaini.kalenderku.util.extentions

inline fun <T> T?.orDefault(
    default: T,
    additionalCheck: (T) -> Boolean = { _ -> true }
): T = if (this != null && additionalCheck(this)) this else default