package com.imaisnaini.kalenderku.ui.util

object
Constant {
    object ContentDescription {
        const val START_ICON = "start icon"
        const val DELETE_ICON = "delete icon"
        const val EDIT_ICON = "edit icon"
        const val END_ICON = "end icon"
        const val STEPPER_ICON = "stepper icon"
        const val SECOND_END_ICON = "second end icon"
        const val PLACEHOLDER_ICON = "placeholder icon"
        const val AVATAR = "avatar"
        const val DESCRIPTION_IMAGE = "placeholder image"
        const val DESCRIPTION_ICON = "description icon"
        const val ACTION_ICON = "action icon"
        const val REFRESH_ICON = "refresh icon"
        const val TOOLTIP_ICON = "tooltip icon"
        const val MODAL_ICON = "modal icon"
        const val LOGO = "logo"
    }

    object DummyContent {
        private const val HTML =
            "<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis " +
                    "egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, " +
                    "ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi " +
                    "vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien " +
                    "ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, " +
                    "ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros " +
                    "ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis " +
                    "pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, " +
                    "tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat " +
                    "volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus," +
                    " metus</p>"

        const val LONG_HTML =
            "<br> $HTML <br> $HTML <br> $HTML <br> $HTML <br> $HTML <br> $HTML"
    }

    const val SCREENSHOT_BACKGROUND_IMAGE_TRANSPARENCY = 0.05f
}
