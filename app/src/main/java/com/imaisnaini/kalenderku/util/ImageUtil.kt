package com.imaisnaini.kalenderku.ui.util

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.ContentScale.Companion.Fit
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp0
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp24
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp48

data class ImageResource(
    val resource: Any,
    val modifier: Modifier = Modifier,
    val scaleType: ContentScale = Fit,
    val colorFilter: ColorFilter? = null
)

fun Modifier.imageSize(width: Dp, height: Dp, radius: Dp = Dp0): Modifier {
    return this
        .height(height)
        .widthIn(Dp0, width)
        .clip(RoundedCornerShape(radius))
        .fillMaxWidth()
}

fun Modifier.imageSize(size: ImageSize) =
    then(Modifier.imageSize(size.width, size.height, size.radius))

sealed class ImageSize(
    open val width: Dp,
    open val height: Dp,
    open val radius: Dp = Dp0
) {

    object ComponentIcon : ImageSize(
        width = Dp24,
        height = Dp24,
        radius = Dp0
    )

    object Icon : ImageSize(
        width = Dp48,
        height = Dp48,
        radius = Dp0
    )

    object XsIllustration : ImageSize(
        width = XsWidth,
        height = XsHeight
    )

    object SIllustration : ImageSize(
        width = SWidth,
        height = SHeight
    )

    object MIllustration : ImageSize(
        width = MWidth,
        height = MHeight
    )

    object LIllustration : ImageSize(
        width = LSize,
        height = LSize,
        radius = Dp0
    )

    data class Custom(override val width: Dp, override val height: Dp, override val radius: Dp) :
        ImageSize(width, height, radius)

    companion object {
        private val XsHeight = 107.dp
        private val XsWidth = 165.dp
        private val SHeight = 150.dp
        private val SWidth = 267.dp
        private val MHeight = 201.dp
        private val MWidth = 327.dp
        private val LSize = 375.dp
    }
}