package com.imaisnaini.kalenderku.util.extentions

fun <T> MutableList<T>.move(from: Int, to: Int) {
    if (from == to || isEmpty()) return
    val element = removeAt(from) ?: return
    add(to, element)
}