package com.imaisnaini.kalenderku.util

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.People
import androidx.compose.material.icons.filled.Person
import androidx.compose.ui.graphics.vector.ImageVector
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.EVENT_ARGS
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.FRIEND_ARGS

sealed class Screen(val route: String, @StringRes val title: Int, val icon: ImageVector? = null) {
    object Splash : Screen("splash", R.string.ScreenSplash)
    object Home : Screen("home", R.string.ScreenHome, Icons.Filled.Home)
    object Login : Screen("login", R.string.ScreenLogin)
    object Signup : Screen("signup", R.string.ScreenRegister)
    object EditProfile : Screen("editProfile", R.string.ScreenProfile)
    object Friend : Screen("friend", R.string.ScreenFriends, Icons.Filled.People)
    object Profile : Screen("profile", R.string.ScreenProfile, Icons.Filled.Person)
    object AddEvent : Screen("addEvent", R.string.ScreenAddEvent)

    object EditEvent : Screen("editEvent/{$EVENT_ARGS}", R.string.ScreenEditEvent)
    object FriendProfile : Screen("friendProfile/{$FRIEND_ARGS}", R.string.ScreenFriends)
}