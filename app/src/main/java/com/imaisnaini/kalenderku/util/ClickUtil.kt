package com.imaisnaini.kalenderku.util

import androidx.compose.foundation.Indication
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.semantics.Role
import com.imaisnaini.kalenderku.util.Constants.FIVE_HUNDRED_LONG
import com.imaisnaini.kalenderku.util.Constants.ZERO_LONG

@Composable
fun rememberDebounceHandler(interval: Long = FIVE_HUNDRED_LONG) = remember { DebounceHandler(interval) }

@Stable
class DebounceHandler(private val interval: Long = FIVE_HUNDRED_LONG) {
    private var lastEventTimeMs = ZERO_LONG

    fun processClick(event: (() -> Unit)?) {
        val now = System.currentTimeMillis()
        if (now - lastEventTimeMs >= interval) event?.invoke()
        lastEventTimeMs = now
    }
}

fun Modifier.clickableSingle(
    enabled: Boolean = true,
    onClickLabel: String? = null,
    role: Role? = null,
    onClick: (() -> Unit)?
) = composed {
    val handler = rememberDebounceHandler()
    Modifier.clickable(
        enabled = enabled,
        onClickLabel = onClickLabel,
        onClick = { handler.processClick(onClick) },
        role = role,
        indication = LocalIndication.current,
        interactionSource = remember { MutableInteractionSource() }
    )
}

fun Modifier.clickableSingle(
    interactionSource: MutableInteractionSource,
    indication: Indication?,
    enabled: Boolean = true,
    onClickLabel: String? = null,
    role: Role? = null,
    onClick: (() -> Unit)?
) = composed {
    val handler = rememberDebounceHandler()
    Modifier.clickable(
        interactionSource = interactionSource,
        indication = indication,
        enabled = enabled,
        onClickLabel = onClickLabel,
        role = role,
    ) { handler.processClick(onClick) }
}