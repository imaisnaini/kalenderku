package com.imaisnaini.kalenderku.util

import com.imaisnaini.kalenderku.util.Constants.FIVE
import com.imaisnaini.kalenderku.util.Constants.FOUR
import com.imaisnaini.kalenderku.util.Constants.ONE
import com.imaisnaini.kalenderku.util.Constants.SEVEN
import com.imaisnaini.kalenderku.util.Constants.SIX
import com.imaisnaini.kalenderku.util.Constants.THREE
import com.imaisnaini.kalenderku.util.Constants.TWO
import com.imaisnaini.kalenderku.util.Constants.ZERO
import com.imaisnaini.kalenderku.util.Constants.ZERO_LONG
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle.FULL
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.util.Date
import java.util.Locale
import java.util.SimpleTimeZone
import java.util.TimeZone
import java.util.TimeZone.getDefault
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.TimeUnit.MINUTES

object DateTimeUtil {

    private const val WIB = "WIB"
    private const val ZONE_ID_LOCAL_DEVICE = "Local/Device"
    private val DEFAULT_ZONE_ID: ZoneId = ZoneId.ofOffset("GMT", ZoneOffset.ofHours(SEVEN))
    const val RAW_FORMAT = "yyyy-MM-dd HH:mm:ss.SSSZ"

    const val DATE_TIME_FORMAT = "dd MM yyyy HH:mm:ss '$WIB'"
    const val SHORT_DATE_FORMAT = "dd MMM yyyy"
    const val LONG_DATE_FORMAT = "dd MMMM yyyy"
    const val TIME_WITH_SERVER_TIME_FORMAT = "HH:mm:ss '$WIB'"
    const val TIME_FORMAT = "HH:mm:ss"
    const val HOUR_MINUTE_WITH_SERVER_TIME_FORMAT = "HH:mm '$WIB'"
    const val HOUR_MINUTE_FORMAT = "HH:mm"
    const val MONTH_NUMBER_FORMAT = "M"
    const val LONG_MONTH_NAME_FORMAT = "MMM"
    const val LONG_MONTH_YEAR = "MMMM yyyy"
    const val MONTH_YEAR_FORMAT = "MMM yyyy"
    const val YEAR_MONTH_DATE = "yyyy-MM-dd"
    const val DATE_MONTH_YEAR = "dd-MM-yyyy"
    const val SHORT_MONTH_YEAR = "MM-yyyy"
    const val DATE_FORMAT = "dd"

    private const val SUNDAY = "SUNDAY"
    private const val MONDAY = "MONDAY"
    private const val TUESDAY = "TUESDAY"
    private const val WEDNESDAY = "WEDNESDAY"
    private const val THURSDAY = "THURSDAY"
    private const val FRIDAY = "FRIDAY"
    private const val SATURDAY = "SATURDAY"

    fun formatDateTime(
        inputDate: String,
        outputFormat: String,
        isLocalTime: Boolean = false,
        inputFormat: String = RAW_FORMAT,
        locale: Locale = Locale.getDefault()
    ): String {
        val input = dateFormat(inputFormat, locale)
        return try {
            input.parse(inputDate)?.let {
                dateFormat(outputFormat, locale)
                    .apply { timeZone = getTimeZone(isLocalTime) }
                    .format(it)
            } ?: inputDate
        } catch (e: Exception) {
            inputDate
        }
    }

    fun getCurrentRawDateTime(): String = dateFormat(RAW_FORMAT)
        .apply { timeZone = getDefault() }
        .format(Date())

    fun getCurrentZonedDateTime() = ZonedDateTime.now(DEFAULT_ZONE_ID)

    fun String.getMillisFromDateTime(format: String = RAW_FORMAT): Long {
        return try {
            dateFormat(format).parse(this)?.toInstant()?.toEpochMilli() ?: ZERO_LONG
        } catch (e: Exception) {
            ZERO_LONG
        }
    }

    fun getCurrentTimeInMillis() = System.currentTimeMillis()

    fun Long.isDateWithinSpecificDaysBeforeCurrentDate(numberOfDay: Int): Boolean {
        val differenceInMillis = this - getCurrentTimeInMillis()
        val daysDifferent = MILLISECONDS.toDays(differenceInMillis)
        return daysDifferent <= numberOfDay
    }

    private fun dateFormat(
        format: String,
        locale: Locale = Locale.getDefault()
    ) = SimpleDateFormat(format, locale)

    private fun getTimeZone(isLocalTime: Boolean): TimeZone {
        return if (isLocalTime) SimpleTimeZone(getDefault().rawOffset, ZONE_ID_LOCAL_DEVICE)
        else TimeZone.getTimeZone(DEFAULT_ZONE_ID)
    }

    fun getRemainingMinuteAndSecond(
        durationInMillis: Long,
        locale: Locale = Locale.getDefault()
    ): String = String.format(
        locale,
        "%02d:%02d",
        MILLISECONDS.toMinutes(durationInMillis),
        MILLISECONDS.toSeconds(durationInMillis) -
                MINUTES.toSeconds(MILLISECONDS.toMinutes(durationInMillis))
    )

    fun YearMonth.getDisplayName(locale: Locale): String {
        return month.getDisplayName(FULL, locale)
    }

    fun YearMonth.getDisplayYear() = "$year"

    fun String.isGreaterThanCurrentDate(inputDateFormat: String = YEAR_MONTH_DATE): Boolean {
        return try {
            val formatter = DateTimeFormatter.ofPattern(inputDateFormat)
            val currentDate = LocalDate.now()
            val inputDate = LocalDate.parse(this, formatter)

            inputDate.isAfter(currentDate)
        } catch (e: Exception) {
            false
        }
    }

    fun String.asLocalDate(pattern: String = YEAR_MONTH_DATE): LocalDate =
        LocalDate.parse(this, DateTimeFormatter.ofPattern(pattern))

    fun LocalDate.formatDate(pattern: String = YEAR_MONTH_DATE): String = format(DateTimeFormatter.ofPattern(pattern))

    fun LocalDateTime.formatDate(pattern: String = YEAR_MONTH_DATE): String = format(DateTimeFormatter.ofPattern(pattern))

    fun ZonedDateTime.formatDate(pattern: String = YEAR_MONTH_DATE): String =
        format(DateTimeFormatter.ofPattern(pattern))

    fun Int.mapIndexDayOfWeek() = when (this) {
        ZERO -> SEVEN
        else -> this
    }

    fun String?.mapDayToDateIndex() = when (this?.uppercase()) {
        MONDAY -> ONE
        TUESDAY -> TWO
        WEDNESDAY -> THREE
        THURSDAY -> FOUR
        FRIDAY -> FIVE
        SATURDAY -> SIX
        SUNDAY -> SEVEN
        else -> null
    }
}
