package com.imaisnaini.kalenderku.util

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object JsonUtil {
    val gson: Gson = Gson()

    fun <T> toJson(data: T): String = gson.toJson(data)

    inline fun <reified T> fromJson(json: String): T? =
        gson.fromJson<T>(json, object : TypeToken<T>() {}.type)
}