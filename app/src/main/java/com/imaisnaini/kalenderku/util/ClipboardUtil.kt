package com.imaisnaini.kalenderku.util

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.ClipboardManager
import androidx.compose.ui.platform.LocalClipboardManager
import androidx.compose.ui.text.AnnotatedString
import com.imaisnaini.kalenderku.util.Constants.ONE
import kotlin.math.abs

object ClipboardUtil {

    @Composable
    fun getClipBoardManager() = LocalClipboardManager.current

    fun copyToClipboard(clipboardManager: ClipboardManager, value: String) =
        clipboardManager.setText(AnnotatedString(value))

    fun isFromClipboard(oldValue: String, newValue: String) =
        abs(oldValue.length - newValue.length) > ONE
}