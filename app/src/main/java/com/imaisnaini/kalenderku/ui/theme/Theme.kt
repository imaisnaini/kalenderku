package com.imaisnaini.kalenderku.ui.theme

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.ReadOnlyComposable
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown400
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800

private val ColorPalette = lightColorScheme(
    primary = Brown800,
    secondary = Brown400,
    tertiary = Brown300,
    background = White,
    surface = White

        /* Other default colors to override
    background = Color(0xFFFFFBFE),
    surface = Color(0xFFFFFBFE),
    onPrimary = Color.White,
    onSecondary = Color.White,
    onTertiary = Color.White,
    onBackground = Color(0xFF1C1B1F),
    onSurface = Color(0xFF1C1B1F),
    */
)

object KalenderkuTheme {
    val typography: MyTypography
        @Composable
        @ReadOnlyComposable
        get() = LocalTypography.current
}
@Composable
fun KalenderkuTheme(
    typography: MyTypography = KalenderkuTheme.typography,
    content: @Composable () -> Unit,
) {
    CompositionLocalProvider(
        LocalTypography provides typography
    ) {
        MaterialTheme(
            colorScheme = ColorPalette,
            content = content
        )
    }
}