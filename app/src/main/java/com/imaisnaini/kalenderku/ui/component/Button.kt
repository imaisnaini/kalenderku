package com.imaisnaini.kalenderku.ui.component

import androidx.compose.foundation.border
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Arrangement.Absolute.spacedBy
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.LocalMinimumInteractiveComponentEnforcement
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults.buttonColors
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.style.TextAlign
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize.Custom
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize.Medium
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp1
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp24
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp32
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp4
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.END_ICON
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.START_ICON
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.rememberDebounceHandler

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Button(
    modifier: Modifier = Modifier,
    text: String = EMPTY_STRING,
    startIcon: ImageVector? = null,
    endIcon: ImageVector? = null,
    type: ButtonType = Primary,
    size: ButtonSize = Medium,
    disabled: Boolean = false,
    onClick: () -> Unit
) {
    val debounceHandler = rememberDebounceHandler()
    val buttonInteractionSource = remember { MutableInteractionSource() }
    val isButtonPressed by buttonInteractionSource.collectIsPressedAsState()
    val buttonShape = RoundedCornerShape(if (size is Custom) size.borderRadius else Dp32)
    CompositionLocalProvider(
        LocalMinimumInteractiveComponentEnforcement provides false
    ) {
        Button(
            onClick = { debounceHandler.processClick(onClick) },
            colors = buttonColors(
                containerColor = when {
                    isButtonPressed -> type.pressedContainerColor
                    else -> type.containerColor
                },
                contentColor = when {
                    isButtonPressed -> type.pressedContentColor
                    else -> type.contentColor
                },
                disabledContainerColor = type.disabledContainerColor,
                disabledContentColor = type.disabledContentColor
            ),
            enabled = !disabled,
            contentPadding = size.paddingValues,
            interactionSource = buttonInteractionSource,
            shape = buttonShape,
            modifier = modifier
                .defaultMinSize(minWidth = Dp24, minHeight = size.minHeight)
                .border(Dp1, type.outlineColor(disabled, isButtonPressed), buttonShape)
        ) {
            Row(
                horizontalArrangement = spacedBy(
                    Dp4,
                    CenterHorizontally
                ),
                verticalAlignment = CenterVertically,
            ) {
                startIcon?.let {
                    Icon(
                        imageVector = it,
                        contentDescription = START_ICON,
                        modifier = Modifier
                            .requiredSize(size.iconSize)
                    )
                }
                if (text.isNotBlank()) {
                    Text(
                        text = text,
                        textAlign = TextAlign.Center,
                        style = size.textStyle(type),
                        modifier = Modifier.wrapContentSize()
                    )
                }
                endIcon?.let {
                    Icon(
                        imageVector = it,
                        contentDescription = END_ICON,
                        modifier = Modifier.requiredSize(size.iconSize)
                    )
                }
            }
        }
    }
}

@Composable
private fun ButtonSize.textStyle(type: ButtonType) = when {
    this is Custom -> textStyle ?: typography.LabelBold1
    this is Medium -> typography.LabelBold1
    else -> typography.LabelBold2
}

private fun ButtonType.outlineColor(disabled: Boolean, pressed: Boolean) = when {
    disabled -> disabledOutlineColor
    pressed -> pressedOutlineColor
    else -> outlineColor
}