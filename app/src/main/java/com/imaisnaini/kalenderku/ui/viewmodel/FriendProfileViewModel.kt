package com.imaisnaini.kalenderku.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.Holiday
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.repository.Repository
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.event.EventResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.holiday.HolidayResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.UserResponse
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.FRIEND_ARGS
import com.imaisnaini.kalenderku.util.DateTimeUtil.SHORT_MONTH_YEAR
import com.imaisnaini.kalenderku.util.DateTimeUtil.formatDate
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class FriendProfileViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val repo: Repository
): ViewModel() {

    private val _eventResponse: MutableLiveData<ApiResult<BaseDTO<List<EventResponse>>>> = MutableLiveData()
    val eventResponse = _eventResponse

    private val _holidayResponse: MutableLiveData<ApiResult<BaseDTO<List<HolidayResponse>>>> = MutableLiveData()
    val holidayResponse = _holidayResponse

    private val _getUserResponse: MutableLiveData<ApiResult<BaseDTO<List<UserResponse>>>> = MutableLiveData()
    val userResponse = _getUserResponse

    val friendPhone: String = checkNotNull(savedStateHandle[FRIEND_ARGS])

    val friendUser = MutableLiveData<User>()
    val eventList = MutableLiveData<List<Event>>(emptyList())
    val holidayList = MutableLiveData<List<Holiday>>(emptyList())

    init {
        getHoliday()
        getUserByPhone(friendPhone)
    }

    fun getEvent(userID: String) = viewModelScope.launch {
        repo.getEvent(
            userID,
            LocalDate.now().formatDate(SHORT_MONTH_YEAR)
        ).collect {
            _eventResponse.value = it
            when(it) {
                is ApiResult.Success -> {
                    eventList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }

                else -> {}
            }
        }
    }
    fun getHoliday() = viewModelScope.launch {
        repo.getHoliday().collect {
            _holidayResponse.value = it
            when(it) {
                is ApiResult.Success -> {
                    holidayList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }

                else -> {}
            }
        }
    }

    fun getUserByPhone(phone: String) = viewModelScope.launch {
        repo.getUserByPhone(phone).collect {
            _getUserResponse.value = it
            when(it) {
                is ApiResult.Success -> {
                    val userTemp = it.data?.data.orEmpty().map { x -> x.mapToLocal() }.first()
                    friendUser.postValue(userTemp)
                    getEvent(userTemp.userID)
                }
                else -> {}
            }
        }
    }
}