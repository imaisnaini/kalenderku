package com.imaisnaini.kalenderku.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.ui.navigation.NavigationHost
import com.imaisnaini.kalenderku.ui.navigation.currentRoute
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.viewmodel.FriendViewModel
import com.imaisnaini.kalenderku.util.Screen
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@OptIn(ExperimentalAnimationApi::class)
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    private val friendViewModel: FriendViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            KalenderkuTheme {
                val navController = rememberNavController()
                val bottomNavigationScreens = listOf(
                    Screen.Home,
                    Screen.Friend,
                    Screen.Profile
                )
                // State of bottomBar, set state to false, if current page route is "car_details"
                val bottomBarState = rememberSaveable { (mutableStateOf(true)) }

                // State of topBar, set state to false, if current page route is "car_details"
                val topBarState = rememberSaveable { (mutableStateOf(true)) }

                val floatingButtonState = rememberSaveable { (mutableStateOf(false)) }

                // Subscribe to navBackStackEntry, required to get current route
                val navBackStackEntry by navController.currentBackStackEntryAsState()

                // Control TopBar and BottomBar
                when (navBackStackEntry?.destination?.route) {
                    Screen.Home.route -> {
                        // Show BottomBar and TopBar
                        bottomBarState.value = true
                        topBarState.value = false
                        floatingButtonState.value = true
                    }
                    Screen.Friend.route -> {
                        // Show BottomBar and TopBar
                        bottomBarState.value = true
                        topBarState.value = true
                        floatingButtonState.value = true
                    }
                    Screen.Profile.route -> {
                        // Show BottomBar and TopBar
                        bottomBarState.value = true
                        topBarState.value = true
                        floatingButtonState.value = false
                    }
                    Screen.EditProfile.route -> {
                        // Show BottomBar and TopBar
                        bottomBarState.value = false
                        topBarState.value = true
                        floatingButtonState.value = false
                    }
                    else -> {
                        // Hide BottomBar and TopBar
                        bottomBarState.value = false
                        topBarState.value = false
                        floatingButtonState.value = false
                    }
                }

                Scaffold(
                    topBar = { TopBar(navController, topBarState) },
                    bottomBar = { AppBottomNavigation(navController, bottomBarState, bottomNavigationScreens) },
                    floatingActionButton = {
                        FloatingButton(floatingButtonState) {
                            when (navBackStackEntry?.destination?.route) {
                                Screen.Home.route -> {
                                    navController.navigate(Screen.AddEvent.route)
                                }

                                Screen.Friend.route -> {
                                    friendViewModel.setAddFriendDialogState(true)
                                    Timber.i("On Click FAB Add Friend")
                                }
                            }
                        }
                    }
                ) { paddingValues ->
                    Column(modifier = Modifier
                        .fillMaxSize()
                        .padding(paddingValues)
                    )  {
                        NavigationHost(navController, friendViewModel)
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@ExperimentalAnimationApi
@Composable
fun TopBar(navController: NavHostController, topBarState: MutableState<Boolean>) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val title: Int = when (navBackStackEntry?.destination?.route ?: Screen.Home.route) {
        Screen.Home.route -> Screen.Home.title
        Screen.Friend.route -> Screen.Friend.title
        Screen.Profile.route -> Screen.Profile.title
        Screen.EditProfile.route -> Screen.EditProfile.title
        Screen.AddEvent.route -> Screen.AddEvent.title
        else -> R.string.app_name
    }

    AnimatedVisibility(
        visible = topBarState.value,
        enter = slideInVertically(initialOffsetY = { -it }),
        exit = slideOutVertically(targetOffsetY = { -it }),
        content = {
            CenterAlignedTopAppBar(
                title = { Text(stringResource(id = title), color = White, style = typography.H3) },
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = Brown800,
                    titleContentColor = White,
                )
            )
        }
    )
}

@Composable
private fun AppBottomNavigation(
    navController: NavHostController,
    bottomBarState: MutableState<Boolean>,
    items: List<Screen>
) {
    AnimatedVisibility(
        visible = bottomBarState.value,
        enter = slideInVertically(initialOffsetY = { it }),
        exit = slideOutVertically(targetOffsetY = { it }),
        content = {
            BottomNavigation(
                backgroundColor = White,
                contentColor = Brown800
            ) {
                val currentRoute = currentRoute(navController)
                items.forEach { screen ->
                    BottomNavigationItem(
                        icon = {
                            screen.icon?.let {
                                Icon(
                                    imageVector = it,
                                    contentDescription = "Screen Icon"
                                )
                            }
                        },
                        label = { Text(screen.route) },
                        selected = currentRoute == screen.route,
                        onClick = {
                            // This if check gives us a "singleTop" behavior where we do not create a
                            // second instance of the composable if we are already on that destination
                            if (currentRoute != screen.route) {
                                navController.navigate(screen.route) {
                                    navController.popBackStack(screen.route, true)
                                }
                            }
                        }
                    )
                }
            }
        }
    )
}

@Composable
fun FloatingButton(
    buttonState: MutableState<Boolean>,
    onClick: () -> Unit
) {
    AnimatedVisibility(
        visible = buttonState.value,
        enter = fadeIn() + slideInVertically(),
        exit = fadeOut() + slideOutVertically()
    ) {
        FloatingActionButton(
            onClick = { onClick() },
            modifier = Modifier.padding(Dp16),
            backgroundColor = Brown300
        ) {
            Icon(Icons.Default.Add, contentDescription = "Add")
        }
    }
}