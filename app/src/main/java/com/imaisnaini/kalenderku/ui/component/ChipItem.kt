package com.imaisnaini.kalenderku.ui.component

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalMinimumInteractiveComponentEnforcement
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.BottomCenter
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import com.imaisnaini.kalenderku.ui.component.ChipAttr.ChipSize
import com.imaisnaini.kalenderku.ui.component.ChipAttr.ChipSize.Small
import com.imaisnaini.kalenderku.ui.theme.BWTones.Black
import com.imaisnaini.kalenderku.ui.theme.BWTones.CharcoalGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.GraniteGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.Gray
import com.imaisnaini.kalenderku.ui.theme.BWTones.PaleGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.SnowWhite
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown400
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.END_ICON
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.START_ICON
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MavChipItem(
    text: String,
    modifier: Modifier = Modifier,
    size: ChipSize = ChipSize.Medium,
    selected: Boolean = false,
    disabled: Boolean = false,
    isFillParent: Boolean = false,
    favLabel: String = EMPTY_STRING,
    startIcon: ImageVector? = null,
    endIcon: ImageVector? = null,
    onClick: () -> Unit
) {
    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()
    val theme = rememberTheme(selected, disabled, isPressed)

    CompositionLocalProvider(LocalMinimumInteractiveComponentEnforcement provides false) {
        Box(
            modifier = if (isFillParent) modifier.fillMaxWidth() else Modifier,
            contentAlignment = BottomCenter
        ) {
            Column {
                OutlinedButton(
                    onClick = onClick,
                    colors = ButtonDefaults.buttonColors(
                        containerColor = theme.containerColor,
                        contentColor = theme.contentColor,
                        disabledContainerColor = PaleGray,
                        disabledContentColor = CharcoalGray
                    ),
                    border = BorderStroke(size.borderSize, theme.outlineColor),
                    enabled = !disabled,
                    contentPadding = PaddingValues(
                        size.horizontalPadding,
                        size.verticalPadding
                    ),
                    interactionSource = interactionSource,
                    shape = RoundedCornerShape(size.borderRadiusSize),
                    modifier = if (isFillParent) {
                        Modifier.fillMaxWidth()
                    } else {
                        modifier.defaultMinSize(minHeight = size.minHeight)
                    }
                ) {
                    Row(
                        horizontalArrangement = Arrangement.spacedBy(
                            size.gapSize,
                            CenterHorizontally
                        ),
                        verticalAlignment = CenterVertically,
                    ) {
                        startIcon?.let {
                            Icon(
                                imageVector = it,
                                contentDescription = START_ICON,
                                tint = theme.contentColor,
                                modifier = Modifier
                                    .size(size.iconSize)
                            )
                        }
                        Text(
                            text = text,
                            color = theme.contentColor,
                            style = if (size is Small) typography.Body3 else typography.Body2,
                        )
                        endIcon?.let {
                            Icon(
                                imageVector = it,
                                contentDescription = END_ICON,
                                tint = theme.contentColor,
                                modifier = Modifier
                                    .size(size.iconSize)
                            )
                        }
                    }
                }
                if (favLabel.isNotBlank()) {
                    Spacer(modifier = Modifier.size(size.favIndentSize))
                }
            }
            if (favLabel.isNotBlank()) {
                Text(
                    text = favLabel,
                    style = typography.Label2,
                    color = theme.favContentColor,
                    modifier = Modifier
                        .background(
                            theme.favContainerColor,
                            shape = RoundedCornerShape(size.favBorderRadiusSize)
                        )
                        .padding(
                            horizontal = size.favHorizontalPadding,
                            vertical = size.favVerticalPadding
                        )
                )
            }
        }
    }
}

@Composable
private fun rememberTheme(
    isSelected: Boolean,
    isDisabled: Boolean,
    isPressed: Boolean,
): ChipItemTheme = remember(isSelected, isPressed) {

    val (containerColor, contentColor, outlineColor: Color) = when {
        isSelected && !isDisabled -> {
            val containerColor = if (!isPressed) Brown400 else Brown300
            val outlineColor = if (!isPressed) Brown400 else Brown300
            listOf(containerColor, White, outlineColor)
        }

        isDisabled -> listOf(SnowWhite, GraniteGray, SnowWhite)
        else -> {
            val containerColor = if (!isPressed) White else Brown400
            val outlineColor = if (!isPressed) Gray else Brown800
            listOf(containerColor, Black, outlineColor)
        }
    }

    val (favContainerColor, favContentColor: Color) = when {
        isDisabled -> listOf(CharcoalGray, SnowWhite)
        else -> listOf(Brown800, White)
    }

    ChipItemTheme(containerColor, contentColor, outlineColor, favContainerColor, favContentColor)
}

private data class ChipItemTheme(
    val containerColor: Color,
    val contentColor: Color,
    val outlineColor: Color,
    val favContainerColor: Color,
    val favContentColor: Color
)