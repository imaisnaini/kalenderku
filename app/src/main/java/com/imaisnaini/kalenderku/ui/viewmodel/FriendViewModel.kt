package com.imaisnaini.kalenderku.ui.viewmodel

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.domain.model.Friend
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.repository.Repository
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.CreateFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.DeleteFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.UpdateStatusFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.friend.CreateFriendResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.friend.GetFriendByUserIDResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.UserResponse
import com.imaisnaini.kalenderku.util.Constants.FriendStatus.ACCEPT
import com.imaisnaini.kalenderku.util.Constants.FriendStatus.PENDING
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FriendViewModel @Inject constructor(
    private val repo: Repository
): ViewModel() {
    private val _getFriendResponse: MutableLiveData<ApiResult<BaseDTO<List<GetFriendByUserIDResponse>>>> = MutableLiveData()
    val friendResponse = _getFriendResponse

    private val _getUserResponse: MutableLiveData<ApiResult<BaseDTO<List<UserResponse>>>> = MutableLiveData()
    val userResponse = _getUserResponse

    private val _createFriendResponse: MutableLiveData<ApiResult<BaseDTO<CreateFriendResponse>>> = MutableLiveData()
    val createFriendResponse = _createFriendResponse

    var friendList = MutableLiveData<List<Friend>>()
    var pendingFriendList = MutableLiveData<List<Friend>>()
    var userList = MutableLiveData<List<User>>()

    private val addFriendDialogState = MutableLiveData(false)
    val getAddFriendDialogState: LiveData<Boolean> = addFriendDialogState
    fun setAddFriendDialogState(state: Boolean) {
        addFriendDialogState.value = state
    }

    // Define a mutable state variable to track whether the toast has been shown
    var toastShown = MutableLiveData(false)

    init {
        getFriends()
        getRequestFriends()
    }
    fun getFriends() = viewModelScope.launch {
        repo.getFriend(ACCEPT).collect {
            _getFriendResponse.value = it
            when(it) {
                is Success -> {
                    friendList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }
                else -> {}
            }
        }
    }

    fun getRequestFriends() = viewModelScope.launch {
        repo.getFriend(PENDING).collect {
            _getFriendResponse.value = it
            when(it) {
                is Success -> {
                    pendingFriendList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }
                else -> {}
            }
        }
    }

    fun postFriendStatus(request: UpdateStatusFriendRequest) = viewModelScope.launch {
        repo.updateStatusFriend(request).collect {
            when(it) {
                is Success -> {
                    getFriends()
                    getRequestFriends()
                }
                else -> {
                    // do something
                }
            }
        }
    }

    fun deleteRequestFriend(request: DeleteFriendRequest) = viewModelScope.launch {
        repo.deleteFriend(request).collect {
            when(it) {
                is Success -> {
                    getFriends()
                    getRequestFriends()
                }
                else -> {
                    // do something
                }
            }
        }
    }

    fun getUserByPhone(phone: String) = viewModelScope.launch {
        repo.getUserByPhone(phone).collect {
            _getUserResponse.value = it
            when(it) {
                is Success -> {
                    userList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }
                else -> {}
            }
        }
    }

    fun postAddFriend(request: CreateFriendRequest) = viewModelScope.launch {
        repo.createFriend(request).collect {
            _createFriendResponse.value = it
        }
    }
}