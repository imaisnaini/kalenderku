package com.imaisnaini.kalenderku.ui.screen

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.Constants.EventStatus.ACCEPT
import com.imaisnaini.kalenderku.bl.Constants.EventStatus.OWNER
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.Holiday
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.ui.MainActivity
import com.imaisnaini.kalenderku.ui.screen.component.DayContent
import com.imaisnaini.kalenderku.ui.screen.component.EventListItem
import com.imaisnaini.kalenderku.ui.screen.component.HolidayColumn
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp1
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp50
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.AVATAR
import com.imaisnaini.kalenderku.ui.viewmodel.FriendProfileViewModel
import com.imaisnaini.kalenderku.util.Constants.ONE_FLOAT
import com.imaisnaini.kalenderku.util.Constants.SPACE_STRING
import io.github.boguszpawlowski.composecalendar.StaticCalendar
import io.github.boguszpawlowski.composecalendar.header.MonthState
import java.time.LocalDate

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FriendProfileScreen(
    navController: NavController,
    friendPhoneNumber: String?,
    viewModel: FriendProfileViewModel = hiltViewModel()
) = with(viewModel) {
    val activity = LocalContext.current as MainActivity
    val friend = friendUser.observeAsState().value
    val events = remember { mutableStateListOf<Event>() }
    eventList.observe(activity) {
        events.apply {
            clear()
            addAll(it)
        }
    }

    val holidays = remember { mutableStateListOf<Holiday>() }
    holidayList.observe(activity) {
        holidays.apply {
            clear()
            addAll(it)
        }
    }

    val currentDate = remember { mutableStateOf(LocalDate.now()) }
    Column {
        CenterAlignedTopAppBar(
            title = { Text(stringResource(id = R.string.ScreenFriends), color = White, style = typography.H3) },
            colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                containerColor = Brown800,
                titleContentColor = White,
                navigationIconContentColor = White
            ),
            navigationIcon = {
                IconButton(onClick = { navController.navigateUp() }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Back",
                        tint = White
                    )
                }
            }
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = Dp8, start = Dp16, end = Dp16),
            horizontalAlignment = CenterHorizontally
        ) {
            friend?.let {
                UserDetail(friend = it)
                Spacer(modifier = Modifier.height(Dp8))
            }
            StaticCalendar (
                modifier = Modifier.border(border = BorderStroke(Dp1, Brown300)),
                dayContent = { DayContent(dayState = it, events = events, holidays = holidays) },
                monthHeader = { Header(monthState = it) }
            )
            holidays.let {
                HolidayColumn(
                    holidays = holidays,
                    month = currentDate.value.month,
                    year = currentDate.value.year
                )
            }
            Spacer(modifier = Modifier.height(Dp8))
            LazyColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(ONE_FLOAT)
            ) {
                items(events) { item ->
                    if (item.status == OWNER || item.status == ACCEPT) {
                        EventListItem(item, onActionClick = {}, onSelected = {})
                    }
                }
            }
        }
    }
}

@Composable
fun UserDetail(
    friend: User
) {
    Row(verticalAlignment = CenterVertically) {
        Icon(
            imageVector = Default.Person,
            contentDescription = AVATAR,
            modifier = Modifier
                .width(Dp50)
                .height(Dp50)
        )
        Column() {
            Text(
                text = friend.fullname,
                style = typography.H3
            )
            Text(text = friend.email)
            Text(text = friend.phone)
        }
    }
}

@Composable
private fun Header(monthState: MonthState) {
    Row(horizontalArrangement = Center,
        verticalAlignment = CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .background(color = Brown300)
            .padding(Dp8)
    ) {
        Text(
            text = monthState.currentMonth.month.name + SPACE_STRING + monthState.currentMonth.year.toString(),
            modifier = Modifier.fillMaxWidth(),
            style = typography.H3,
            textAlign = TextAlign.Center
        )
    }
}