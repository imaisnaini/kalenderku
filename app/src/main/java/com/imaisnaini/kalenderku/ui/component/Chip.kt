package com.imaisnaini.kalenderku.ui.component

import androidx.compose.foundation.layout.Arrangement.spacedBy
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import com.imaisnaini.kalenderku.ui.component.ChipAttr.ChipItem
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.util.Constants.TWO

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun <T> Chip(
    items: List<ChipItem<T>>,
    modifier: Modifier = Modifier,
    gap: Dp = Dp8,
    selectedItems: SnapshotStateList<T>,
    selectedCondition: ((selected: T, item: T) -> Boolean)? = null,
    onItemSelected: ((chip: ChipItem<T>) -> Unit)
) {
    FlowRow(
        horizontalArrangement = spacedBy(gap),
    ) {
        items.forEachIndexed { index, item ->
            SelectionChipItem(
                item = item,
                verticalGap = gap.div(TWO),
                selectedItems = selectedItems,
                selectedCondition = selectedCondition,
                onItemSelected = onItemSelected,
            )
        }
    }
}

@Composable
private fun <T> SelectionChipItem(
    item: ChipItem<T>,
    verticalGap: Dp? = null,
    selectedItems: SnapshotStateList<T>,
    selectedCondition: ((selected: T, item: T) -> Boolean)? = null,
    onItemSelected: ((chip: ChipItem<T>) -> Unit)
) {
    val selected = if (selectedCondition != null) {
        selectedItems.find { selectedCondition(item.value, it) } != null
    } else selectedItems.contains(item.value)

    MavChipItem(
        modifier = Modifier.let { if (verticalGap != null) it.padding(vertical = verticalGap) else it },
        text = item.label,
        startIcon = item.startIcon,
        endIcon = item.endIcon,
        selected = selected,
        disabled = item.isDisabled,
        size = item.size,
        favLabel = item.favLabel,
    ) {
        onItemSelected(item)
    }
}