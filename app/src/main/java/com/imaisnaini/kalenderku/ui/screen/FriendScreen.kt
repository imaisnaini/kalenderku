package com.imaisnaini.kalenderku.ui.screen

import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.spacedBy
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.AlertDialog
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material3.Card
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Alignment.Companion.End
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextAlign.Companion.Start
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.ui.MainActivity
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.api.ApiResult.*
import com.imaisnaini.kalenderku.bl.data.domain.model.Friend
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.CreateFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.DeleteFriendRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.friend.UpdateStatusFriendRequest
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize.Small
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Secondary
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.FRIEND_ARGS
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp400
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.AVATAR
import com.imaisnaini.kalenderku.ui.util.ImageSize.Icon
import com.imaisnaini.kalenderku.ui.viewmodel.FriendViewModel
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Constants.FriendStatus.ACCEPT
import com.imaisnaini.kalenderku.util.Constants.THREE
import com.imaisnaini.kalenderku.util.Screen.FriendProfile
import androidx.compose.material.icons.filled.AccountCircle as AccountCircle1

@Composable
fun FriendScreen(
    navController: NavController,
    viewModel: FriendViewModel
) = with(viewModel) {

    val friends = remember { mutableStateListOf<Friend>() }
    friendList.observe(LocalContext.current as MainActivity) {
        friends.apply {
            clear()
            addAll(it)
        }
    }

    val pendingFriends = remember { mutableStateListOf<Friend>() }
    pendingFriendList.observe(LocalContext.current as MainActivity) {
        pendingFriends.apply {
            clear()
            addAll(it)
        }
    }

    val addFriendResponse = createFriendResponse.observeAsState().value
    when(addFriendResponse) {
        is Success -> {
            if(toastShown.value == false) {
                Toast.makeText(LocalContext.current, "Request Teman Telah Dikirim", LENGTH_SHORT).show()
                toastShown.value = true
            }
        }
        is Error -> {
            if(toastShown.value == false) {
                Toast.makeText(LocalContext.current, "Request Teman Gagal", LENGTH_SHORT).show()
                toastShown.value = true
            }
        }
        else -> {}
    }

    val dialogState = remember { mutableStateOf(false) }
    getAddFriendDialogState.observe(LocalContext.current as MainActivity) {
        dialogState.value = it
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(Dp16)
    ) {
        if(dialogState.value == true) {
            AddUserDialog(viewModel = viewModel)
        }

        pendingFriends.let {
            if (it.isNotEmpty()) {
                Text(text = "Teman Baru",
                    style = typography.BodyBold1,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = Dp16),
                    textAlign = Start
                )
            }
            LazyColumn {
                items(it) {friend ->
                    RequestFriendItem(
                        friend,
                        onAccept = {
                            val request = UpdateStatusFriendRequest(
                                friendUserID = friend.friendUserID,
                                status = ACCEPT
                            )
                            postFriendStatus(request)
                        },
                        onReject = {
                            val request = DeleteFriendRequest(friend.friendUserID)
                            deleteRequestFriend(request)
                        }
                    )
                }
            }
        }
        if (friends.isNotEmpty() && pendingFriends.isNotEmpty()) {
            Divider()
            Spacer(modifier = Modifier.height(Dp16))
        }
        friends.let {
            if (it.isNotEmpty()) {
                Text(text = "Teman",
                    style = typography.BodyBold1,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = Dp16),
                    textAlign = Start
                )
            }
            LazyColumn {
                items(it) {friend ->
                    FriendItem(friend) {
                        navController.navigate(FriendProfile.route.replace("{$FRIEND_ARGS}", friend.phone))
                    }
                }
            }
        }
        if(friends.isEmpty() && pendingFriends.isEmpty()) {
            Text(text = "Kamu belum punya teman",
                modifier = Modifier
                    .padding(vertical = Dp16)
                    .fillMaxWidth(),
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun FriendItem(
    friend: Friend,
    onClick: () -> Unit
) {
    Card(modifier = Modifier
        .fillMaxWidth()
        .padding(bottom = Dp8)
        .clickable { onClick() }
    ) {
        Row(
            verticalAlignment = CenterVertically,
            horizontalArrangement = spacedBy(Dp16),
            modifier = Modifier
                .fillMaxWidth()
                .padding(Dp8)
        ) {
            Icon(
                imageVector = Default.AccountCircle1,
                contentDescription = AVATAR,
                modifier = Modifier.size(Icon.width, Icon.height)
            )
            Text(text = friend.fullname)
        }
    }
}

@Composable
fun RequestFriendItem(
    friend: Friend,
    onAccept: () -> Unit,
    onReject: () -> Unit
) {
    Card(modifier = Modifier
        .fillMaxWidth()
        .padding(bottom = Dp8)) {
        Row(
            verticalAlignment = CenterVertically,
            horizontalArrangement = spacedBy(Dp16),
            modifier = Modifier
                .fillMaxWidth()
                .padding(Dp8)
        ) {
            Icon(
                imageVector = Default.AccountCircle1,
                contentDescription = AVATAR,
                modifier = Modifier.size(Icon.width, Icon.height)
            )
            Column(
                verticalArrangement = spacedBy(Dp8),
                modifier = Modifier.fillMaxWidth()
            ) {
                Text(text = friend.fullname)
                Row(horizontalArrangement = spacedBy(Dp8)) {
                    Button(
                        text = stringResource(id = R.string.ActionAccept),
                        type = Primary,
                        size = Small
                    ) { onAccept() }
                    Button(
                        text = stringResource(id = R.string.ActionReject),
                        type = Secondary,
                        size = Small
                    ) { onReject() }
                }
            }
        }
    }
}
@Composable
fun AddUserDialog(viewModel: FriendViewModel) = with(viewModel) {
    var phoneNumber by remember { mutableStateOf(EMPTY_STRING) }
    val usersFound = remember { mutableStateListOf<User>() }

    userList.observe(LocalContext.current as MainActivity) {
        usersFound.apply {
            clear()
            addAll(it)
        }
    }

    AlertDialog(
        onDismissRequest = { setAddFriendDialogState(false) },
        title = { Text("Tambah Teman") },
        text = {
            Column(Modifier.height(Dp400)) {
                OutlinedTextField(
                    value = phoneNumber,
                    onValueChange = { newPhoneNumber ->
                        phoneNumber = newPhoneNumber
                        // Trigger search whenever phone number changes
                        if(newPhoneNumber.length > THREE) {
                            getUserByPhone(newPhoneNumber)
                        }
                    },
                    label = { Text("Masukkan nomor telephone") },
                    modifier = Modifier.padding(bottom = Dp8)
                )
                LazyColumn {
                    items(usersFound) { user ->
                        Card(modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = Dp8)
                        ) {
                            Column(horizontalAlignment = End) {
                                Row(
                                    verticalAlignment = CenterVertically,
                                    horizontalArrangement = spacedBy(Dp16),
                                    modifier = Modifier.fillMaxWidth()
                                ) {
                                    Icon(
                                        imageVector = Default.AccountCircle1,
                                        contentDescription = AVATAR,
                                        modifier = Modifier.size(Icon.width, Icon.height)
                                    )
                                    Text(text = user.fullname)
                                }
                                Button(
                                    text = "Request Teman",
                                    type = Primary,
                                    size = Small
                                ) {
                                    postAddFriend(CreateFriendRequest(friendUserID = user.userID))
                                    setAddFriendDialogState(false)
                                }
                            }
                        }
                    }
                }
                if(usersFound.isEmpty()) {
                    Text(text = "User dengan nomor telephon $phoneNumber tidak ditemukan")
                }
            }
        },
        confirmButton = {},
        dismissButton = {
            TextButton(
                onClick = { setAddFriendDialogState(false) }
            ) {
                androidx.compose.material.Text(stringResource(id = R.string.ActionClose))
            }
        }
    )
}
