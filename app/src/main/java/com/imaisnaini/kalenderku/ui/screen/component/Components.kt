package com.imaisnaini.kalenderku.ui.screen.component

import android.graphics.Color.parseColor
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Arrangement.End
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.Constants.EventStatus.ACCEPT
import com.imaisnaini.kalenderku.bl.Constants.EventStatus.PENDING
import com.imaisnaini.kalenderku.bl.Constants.EventStatus.REJECT
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.Holiday
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize.Small
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Secondary
import com.imaisnaini.kalenderku.ui.theme.BWTones
import com.imaisnaini.kalenderku.ui.theme.BWTones.LightGray
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp0
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp1
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp2
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp4
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.util.ImageSize
import com.imaisnaini.kalenderku.util.Constants.COLON
import com.imaisnaini.kalenderku.util.Constants.ONE_FLOAT
import com.imaisnaini.kalenderku.util.Constants.SPACE_STRING
import com.imaisnaini.kalenderku.util.DateTimeUtil.DATE_FORMAT
import com.imaisnaini.kalenderku.util.DateTimeUtil.LONG_DATE_FORMAT
import com.imaisnaini.kalenderku.util.DateTimeUtil.formatDate
import com.imaisnaini.kalenderku.util.extentions.capitalizeEachWord
import io.github.boguszpawlowski.composecalendar.day.NonSelectableDayState
import java.time.DayOfWeek
import java.time.Month


@Composable
fun DayContent(
    dayState: NonSelectableDayState,
    events: List<Event>,
    holidays: List<Holiday>
) = with(dayState) {
    val isHoliday = holidays.find { it.holidayDate == date }
    val eventExist = events.find { it.eventDate == date }
    val dayBackground = if (eventExist != null) Color(parseColor(eventExist.color)) else MaterialTheme.colors.surface
    val isSunday = (date.dayOfWeek == DayOfWeek.SUNDAY)

    Card(
        modifier = Modifier
            .aspectRatio(ONE_FLOAT)
            .padding(Dp2),
        elevation = if (dayState.isFromCurrentMonth) Dp4 else Dp0,
        border = if (dayState.isCurrentDay) BorderStroke(Dp1, Brown300) else null,
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.background(dayBackground)
        ) {
            Text(
                text = date.dayOfMonth.toString(),
                color = (if (isHoliday != null || isSunday) Color.Red else BWTones.Black)
            )
        }
    }
}

@Composable
fun HolidayColumn(
    month: Month,
    year: Int,
    holidays: List<Holiday>
) {
    Column(horizontalAlignment = CenterHorizontally,
        verticalArrangement = Center,
        modifier = Modifier
            .fillMaxWidth()
            .border(border = BorderStroke(width = Dp1, LightGray))
            .padding(Dp8)
    ) {
        LazyColumn {
            items(holidays) {
                if (it.holidayDate.year == year && it.holidayDate.month == month) {
                    val date = it.holidayDate.formatDate(DATE_FORMAT)
                    Text(text = "$date$COLON$SPACE_STRING${it.name}", color = Color.Red)
                }
            }
        }
    }
}

@Composable
fun EventListItem(
    event: Event,
    onActionClick: (String) -> Unit,
    onSelected: () -> Unit
) {
    val eventColor = Color(parseColor(event.color))
    Card(
        modifier = Modifier.fillMaxWidth()
            .clickable { onSelected() }
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(IntrinsicSize.Min)) {
            Box(modifier = Modifier
                .width(ImageSize.Icon.width)
                .fillMaxHeight()
                .background(color = eventColor))
            Column(
                verticalArrangement = Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = Dp8, vertical = Dp4)
            ) {
                Text(event.eventDate.formatDate(LONG_DATE_FORMAT))
                Text(event.name.capitalizeEachWord())
                if (event.status == PENDING) {
                    Row(horizontalArrangement = End,
                        modifier = Modifier.fillMaxWidth()) {
                        Button(
                            text = stringResource(id = R.string.ActionAccept),
                            type = Primary,
                            size = Small
                        ) { onActionClick(ACCEPT) }
                        Spacer(modifier = Modifier.width(Dp8))
                        Button(
                            text = stringResource(id = R.string.ActionReject),
                            type = Secondary,
                            size = Small
                        ) { onActionClick(REJECT) }
                    }
                }
            }
        }
    }
    Spacer(modifier = Modifier.height(Dp4))
}