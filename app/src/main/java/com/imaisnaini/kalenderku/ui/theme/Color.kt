package com.imaisnaini.kalenderku.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

object Browns {
    val Brown800 = Color(0xFF3E3232)
    val Brown400 = Color(0xFF7E6363)
    val Brown300 = Color(0xFFA87C7C)
    val Brown100 = Color(0xFFC7C0C0)
}


object CalenderColor {
    val Turquoise = Color(0xFF4DB6AC)
    val Orange = Color(0xFFFFAB91)
    val Red = Color(0xFFE57373)
    val Yellow = Color(0xFFFFF59D)
    val Pink = Color(0xFFCE93D8)
    val Magenta = Color(0xFF9575CD)
    val Purple = Color(0xFF6A4AB2)
    val Green = Color(0xFF81C784)
    val Blue = Color(0xFF80DEEA)
}

object BWTones {
    val Black = Color(0xFF0E0E0E)
    val CharcoalGray = Color(0xFF4E4E4E)
    val GraniteGray = Color(0xFF7A7A7A)
    val Gray = Color(0xFFA0A0A0)
    val LightGray = Color(0xFFC6C6C6)
    val PaleGray = Color(0xFFDADADA)
    val SnowWhite = Color(0xFFF0F0F0)
    val OffWhite = Color(0xFFF9F9F9)
    val White = Color(0xFFFFFFFF)
}

object Success {
    val Success1 = Color(0xFFE1FEF7)
    val Success2 = Color(0xFF02C794)
}

object Error {
    val Error1 = Color(0xFFFFEAEA)
    val Error2 = Color(0xFFD65C5C)
}

object Informative {
    val Informative1 = Color(0xFFFFFFFF)
    val Informative2 = Color(0xFF017A71)
}

object Warning {
    val Warning1 = Color(0xFFFFF5DB)
    val Warning2 = Color(0xFFEBBC44)
}