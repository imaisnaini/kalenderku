package com.imaisnaini.kalenderku.ui.component

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize.Medium.iconSize
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonSize.Medium.paddingValues
import com.imaisnaini.kalenderku.ui.theme.BWTones.Black
import com.imaisnaini.kalenderku.ui.theme.BWTones.GraniteGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.LightGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.OffWhite
import com.imaisnaini.kalenderku.ui.theme.BWTones.SnowWhite
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown400
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp12
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp24
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp32
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp48
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

object ButtonAttr {

    sealed class ButtonType(
        val containerColor: Color = Brown800,
        val outlineColor: Color = Transparent,
        val contentColor: Color = White,
        val pressedContainerColor: Color = Brown400,
        val pressedOutlineColor: Color = Transparent,
        val pressedContentColor: Color = White,
        val disabledContainerColor: Color = SnowWhite,
        val disabledOutlineColor: Color = Transparent,
        val disabledContentColor: Color = GraniteGray
    ) {

        object Primary : ButtonType()

        object Secondary : ButtonType(
            containerColor = Brown400,
            outlineColor = Transparent,
            pressedContainerColor = Brown300,
            pressedOutlineColor = Transparent,
            disabledContainerColor = White,
            disabledOutlineColor = Transparent
        )

        data class Custom(
            val container: Color = Brown800,
            val outline: Color = Brown800,
            val content: Color = Black,
            val pressedContainer: Color = Brown400,
            val pressedOutline: Color = Brown400,
            val pressedContent: Color = Black,
            val disabledContainer: Color = SnowWhite,
            val disabledOutline: Color = SnowWhite,
            val disabledContent: Color = GraniteGray
        ) : ButtonType(
            containerColor = container,
            outlineColor = outline,
            contentColor = content,
            pressedContainerColor = pressedContainer,
            pressedOutlineColor = pressedOutline,
            pressedContentColor = pressedContent,
            disabledContainerColor = disabledContainer,
            disabledOutlineColor = disabledOutline,
            disabledContentColor = disabledContent
        )
    }

    sealed class ButtonSize(
        val paddingValues: PaddingValues = PaddingValues(Dp32, Dp12),
        val minHeight: Dp = Dp32,
        val iconSize: Dp = Dp24
    ) {
        object Medium : ButtonSize(
            minHeight = Dp48
        )

        object Small : ButtonSize(
            paddingValues = PaddingValues(Dp24, Dp8),
            minHeight = Dp32,
            iconSize = Dp16,
        )

        data class Custom(
            val padding: PaddingValues = paddingValues,
            val icon: Dp = iconSize,
            val borderRadius: Dp = Dp32,
            val textStyle: TextStyle? = null,
            val size: Dp = iconSize
        ) : ButtonSize(padding, icon, size)
    }
}