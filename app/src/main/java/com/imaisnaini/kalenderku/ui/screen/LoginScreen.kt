package com.imaisnaini.kalenderku.ui.screen

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.Icons.Filled
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.ImeAction.Companion.Done
import androidx.compose.ui.text.input.ImeAction.Companion.Next
import androidx.compose.ui.text.input.KeyboardCapitalization.Companion.None
import androidx.compose.ui.text.input.KeyboardType.Companion.Password
import androidx.compose.ui.text.input.KeyboardType.Companion.Phone
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.api.ApiResult.Error
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.remote.request.user.LoginRequest
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Custom
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp120
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp4
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.LOGO
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.START_ICON
import com.imaisnaini.kalenderku.ui.viewmodel.MainViewModel
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Constants.ONE_FLOAT
import com.imaisnaini.kalenderku.util.Screen.Home
import com.imaisnaini.kalenderku.util.Screen.Signup

@Composable
fun LoginScreen(
    navController: NavController,
    viewModel: MainViewModel = hiltViewModel()
) = with(viewModel) {
    var phone by remember { mutableStateOf(EMPTY_STRING) }
    var password by remember { mutableStateOf(EMPTY_STRING) }
    var passwordVisible by remember { mutableStateOf(false) }

    val context = LocalContext.current
    val loginState = loginResponse.observeAsState()
    var isSuccessSaved by remember {
        mutableStateOf(false)
    }

    loginState.value.let {
        when(it) {
            is Success -> {
                if (!isSuccessSaved) {
                    it.data?.data?.let { it1 ->
                        saveLogin(it1.token, it1.mapToLocal())
                        navController.navigate(Home.route)
                    }
                    isSuccessSaved = true
                }
            }
            is Error -> Toast.makeText(context, it.error, Toast.LENGTH_SHORT).show()
            else -> {}
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(Dp16),
        verticalArrangement = Center,
        horizontalAlignment = CenterHorizontally
    ) {
        Spacer(modifier = Modifier.weight(ONE_FLOAT))
        // App logo or branding image
        Image(
            painter = painterResource(id = R.drawable.ic_logo),
            contentDescription = LOGO,
            modifier = Modifier
                .size(Dp120)
                .wrapContentSize(Alignment.Center)
        )

        Spacer(modifier = Modifier.height(Dp16))

        // Email TextField
        OutlinedTextField(
            value = phone,
            onValueChange = { phone = it },
            label = { Text(stringResource(id = R.string.TextPhoneNumber)) },
            leadingIcon = {
                Icon(
                    imageVector = Default.Phone,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = Phone,
                capitalization = None,
                imeAction = Next
            )
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Password TextField
        OutlinedTextField(
            value = password,
            onValueChange = { password = it },
            label = { Text(stringResource(id = R.string.TextPassword)) },
            leadingIcon = {
                Icon(
                    imageVector = Default.Lock,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = Password,
                capitalization = None,
                imeAction = Done
            ),
            trailingIcon = {
                IconButton(
                    onClick = { passwordVisible = !passwordVisible }
                ) {
                    Icon(
                        if (passwordVisible) Filled.Visibility else Filled.VisibilityOff,
                        contentDescription = "Toggle password visibility"
                    )
                }
            },
            visualTransformation = when {
                passwordVisible -> VisualTransformation.None
                else -> PasswordVisualTransformation()
            },
        )

        Spacer(modifier = Modifier.height(Dp16))

        // Login Button
        Button(
            text = stringResource(id = R.string.ActionLogin),
            type = Primary,
        ) {
            when {
                (phone.isNotEmpty() && password.isNotEmpty()) -> {
                    postLogin(LoginRequest(phone, password))
                }
                else -> {
                    Toast.makeText(context, "Harap lengkapi form!", Toast.LENGTH_SHORT).show()
                }
            }
        }

        Spacer(modifier = Modifier.weight(ONE_FLOAT))


        Text("Belum punya akun?",
            modifier = Modifier.padding(bottom = Dp4))
        Button(
            text = stringResource(id = R.string.ActionResigter),
            type = Custom(
                container = White,
                content = Brown800
            ),
        ) {
            navController.navigate(Signup.route)
        }
    }
}