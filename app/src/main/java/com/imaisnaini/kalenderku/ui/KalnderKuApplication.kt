package com.imaisnaini.kalenderku.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import timber.log.Timber.Forest.plant
import timber.log.Timber.Forest.uprootAll
import com.imaisnaini.kalenderku.BuildConfig

@HiltAndroidApp
class KalnderKuApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        initTimber()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) plant(Timber.DebugTree()) else uprootAll()
    }

}