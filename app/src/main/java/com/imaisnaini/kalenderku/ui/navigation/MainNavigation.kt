package com.imaisnaini.kalenderku.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.navArgument
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.EVENT_ARGS
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.FRIEND_ARGS
import com.imaisnaini.kalenderku.ui.screen.AddEventScreen
import com.imaisnaini.kalenderku.ui.screen.EditProfileScreen
import com.imaisnaini.kalenderku.ui.screen.FriendProfileScreen
import com.imaisnaini.kalenderku.ui.screen.FriendScreen
import com.imaisnaini.kalenderku.ui.screen.HomeScreen
import com.imaisnaini.kalenderku.ui.screen.LoginScreen
import com.imaisnaini.kalenderku.ui.screen.ProfileScreen
import com.imaisnaini.kalenderku.ui.screen.SignupScreen
import com.imaisnaini.kalenderku.ui.screen.SplashScreen
import com.imaisnaini.kalenderku.ui.viewmodel.FriendViewModel
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Screen


@Composable
fun NavigationHost(navController: NavHostController, viewModel: FriendViewModel) {
    NavHost(
        navController = navController,
        startDestination = Screen.Splash.route
    ) {
        composable(Screen.Splash.route) {
            SplashScreen(navController)
        }
        composable(Screen.Home.route) {
            HomeScreen(navController)
        }
        composable(Screen.Login.route) {
            LoginScreen(navController)
        }
        composable(Screen.Signup.route) {
            SignupScreen(navController)
        }
        composable(Screen.Profile.route) {
            ProfileScreen(navController)
        }
        composable(Screen.EditProfile.route) {
            EditProfileScreen(navController)
        }
        composable(Screen.Friend.route) {
            FriendScreen(navController, viewModel)
        }
        composable(Screen.AddEvent.route) {
            AddEventScreen(navController)
        }
        composable(
            route = Screen.EditEvent.route,
            arguments = listOf(navArgument(EVENT_ARGS) {
                defaultValue = EMPTY_STRING
                type = NavType.StringType
            })
        ) {
            AddEventScreen(navController, it.arguments?.getString(EVENT_ARGS))
        }
        composable(
            route = Screen.FriendProfile.route,
            arguments = listOf(navArgument(FRIEND_ARGS) {
                defaultValue = EMPTY_STRING
                type = NavType.StringType
            })
        ) {
            FriendProfileScreen(navController, it.arguments?.getString(FRIEND_ARGS))
        }
    }
}

@Composable
fun currentRoute(navController: NavHostController): String? {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    return navBackStackEntry?.arguments?.getString(Screen.Home.route)
}

object NavArgs {
    const val FRIEND_ARGS = "friendPhoneNumber"
    const val EVENT_ARGS = "eventId"
}