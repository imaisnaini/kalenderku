package com.imaisnaini.kalenderku.ui.theme

import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.ui.theme.TextDimen


private val interphases = FontFamily(
        Font(R.font.interphases_regular, FontWeight.Normal),
        Font(R.font.interphases_medium, FontWeight.Medium),
        Font(R.font.interphases_bold, FontWeight.Bold),
        Font(R.font.interphases_demibold, FontWeight.SemiBold),
        Font(R.font.interphases_extrabold, FontWeight.ExtraBold)
)

private const val HEADLINE_LINE_HEIGHT = 1.2
private const val BODY_LINE_HEIGHT = 1.4
private const val LABEL_LINE_HEIGHT = 1.2
private const val AMOUNT_LINE_HEIGHT = 1.2
private const val AVATAR_LINE_HEIGHT = 1.2
private const val UNDERLINE_LINE_HEIGHT = 1.2

data class MyTypography(
        val H0: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.ExtraBold,
                fontSize = TextDimen.Sp32,
                lineHeight = TextDimen.Sp32 * HEADLINE_LINE_HEIGHT
        ),
        val H1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.ExtraBold,
                fontSize = TextDimen.Sp28,
                lineHeight = TextDimen.Sp28 * HEADLINE_LINE_HEIGHT
        ),
        val H2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp24,
                lineHeight = TextDimen.Sp24 * HEADLINE_LINE_HEIGHT
        ),
        val H3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp18,
                lineHeight = TextDimen.Sp18 * HEADLINE_LINE_HEIGHT
        ),
        val H4: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp16,
                lineHeight = TextDimen.Sp16 * HEADLINE_LINE_HEIGHT
        ),
        val H5: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * HEADLINE_LINE_HEIGHT
        ),
        val H6: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * HEADLINE_LINE_HEIGHT
        ),
        val Body1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Normal,
                fontSize = TextDimen.Sp16,
                lineHeight = TextDimen.Sp16 * BODY_LINE_HEIGHT
        ),
        val BodyBold1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp16,
                lineHeight = TextDimen.Sp16 * BODY_LINE_HEIGHT
        ),
        val Body2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Normal,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * BODY_LINE_HEIGHT
        ),
        val BodyBold2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * BODY_LINE_HEIGHT
        ),
        val Body3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Normal,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * BODY_LINE_HEIGHT
        ),
        val BodyBold3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * BODY_LINE_HEIGHT
        ),
        val Label1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Normal,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * LABEL_LINE_HEIGHT
        ),
        val LabelBold1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * LABEL_LINE_HEIGHT
        ),
        val Label2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Normal,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * LABEL_LINE_HEIGHT
        ),
        val LabelBold2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * LABEL_LINE_HEIGHT
        ),
        val Label3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Normal,
                fontSize = TextDimen.Sp10,
                lineHeight = TextDimen.Sp10 * LABEL_LINE_HEIGHT
        ),
        val LabelBold3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp10,
                lineHeight = TextDimen.Sp10 * LABEL_LINE_HEIGHT
        ),
        val Amount1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.ExtraBold,
                fontSize = TextDimen.Sp40,
                lineHeight = TextDimen.Sp40 * AMOUNT_LINE_HEIGHT
        ),
        val Amount2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.ExtraBold,
                fontSize = TextDimen.Sp28,
                lineHeight = TextDimen.Sp28 * AMOUNT_LINE_HEIGHT
        ),
        val Amount3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.ExtraBold,
                fontSize = TextDimen.Sp20,
                lineHeight = TextDimen.Sp20 * AMOUNT_LINE_HEIGHT
        ),
        val Avatar1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.SemiBold,
                fontSize = TextDimen.Sp18,
                lineHeight = TextDimen.Sp18 * AVATAR_LINE_HEIGHT
        ),
        val Underline1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Medium,
                fontSize = TextDimen.Sp16,
                lineHeight = TextDimen.Sp16 * UNDERLINE_LINE_HEIGHT,
                textDecoration = TextDecoration.Underline
        ),
        val UnderlineBold1: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp16,
                lineHeight = TextDimen.Sp16 * UNDERLINE_LINE_HEIGHT,
                textDecoration = TextDecoration.Underline
        ),
        val Underline2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Medium,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * UNDERLINE_LINE_HEIGHT,
                textDecoration = TextDecoration.Underline
        ),
        val UnderlineBold2: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp14,
                lineHeight = TextDimen.Sp14 * UNDERLINE_LINE_HEIGHT,
                textDecoration = TextDecoration.Underline
        ),
        val Underline3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Medium,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * UNDERLINE_LINE_HEIGHT,
                textDecoration = TextDecoration.Underline
        ),
        val UnderlineBold3: TextStyle = TextStyle(
                fontFamily = interphases,
                fontWeight = FontWeight.Bold,
                fontSize = TextDimen.Sp12,
                lineHeight = TextDimen.Sp12 * UNDERLINE_LINE_HEIGHT,
                textDecoration = TextDecoration.Underline
        )
)

internal val LocalTypography = staticCompositionLocalOf { MyTypography() }