package com.imaisnaini.kalenderku.ui.screen

import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.Icons.Filled
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.ImeAction.Companion.Done
import androidx.compose.ui.text.input.ImeAction.Companion.Next
import androidx.compose.ui.text.input.KeyboardCapitalization.Companion.None
import androidx.compose.ui.text.input.KeyboardType.Companion.Email
import androidx.compose.ui.text.input.KeyboardType.Companion.Password
import androidx.compose.ui.text.input.KeyboardType.Companion.Phone
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.api.ApiResult.Error
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.remote.request.user.CreateUserRequest
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp24
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.START_ICON
import com.imaisnaini.kalenderku.ui.viewmodel.MainViewModel
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Screen
import com.imaisnaini.kalenderku.util.Screen.Login

@Composable
fun SignupScreen(
    navController: NavController,
    viewModel: MainViewModel = hiltViewModel()
) = with(viewModel) {
    var name by remember { mutableStateOf(EMPTY_STRING) }
    var email by remember { mutableStateOf(EMPTY_STRING) }
    var phoneNumber by remember { mutableStateOf(EMPTY_STRING) }
    var password by remember { mutableStateOf(EMPTY_STRING) }
    var passwordVisible by remember { mutableStateOf(false) }

    val context = LocalContext.current
    val registerState = registerResponse.observeAsState()

    registerState.value.let {
        when(it) {
            is Success -> {
                navController.navigateUp()
            }
            is Error -> Toast.makeText(context, it.error, LENGTH_SHORT).show()
            else -> {}
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(Dp16),
        verticalArrangement = Center,
        horizontalAlignment = CenterHorizontally
    ) {
        Text(
            text = stringResource(id = R.string.ScreenRegister),
            style = typography.H2,
            color = Brown800
        )
        Spacer(modifier = Modifier.height(Dp24))

        // Name TextField
        OutlinedTextField(
            value = name,
            onValueChange = { name = it },
            label = { Text(stringResource(id = R.string.TextFullName)) },
            leadingIcon = {
                Icon(
                    imageVector = Default.Person,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(imeAction = Next)
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Email TextField
        OutlinedTextField(
            value = email,
            onValueChange = { email = it },
            label = { Text(stringResource(id = R.string.TextEmail)) },
            leadingIcon = {
                Icon(
                    imageVector = Default.Email,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = Email,
                capitalization = None,
                imeAction = Next
            )
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Phone Number TextField
        OutlinedTextField(
            value = phoneNumber,
            onValueChange = { phoneNumber = it },
            label = { Text(stringResource(id = R.string.TextPhoneNumber)) },
            leadingIcon = {
                Icon(
                    imageVector = Default.Phone,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = Phone,
                capitalization = None,
                imeAction = Next
            )
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Password TextField
        OutlinedTextField(
            value = password,
            onValueChange = { password = it },
            label = { Text(stringResource(id = R.string.TextPassword)) },
            leadingIcon = {
                Icon(
                    imageVector = Default.Lock,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(
                keyboardType = Password,
                capitalization = None,
                imeAction = Done
            ),
            trailingIcon = {
                IconButton(onClick = { passwordVisible = !passwordVisible }) {
                    Icon(
                        if (passwordVisible) Filled.Visibility else Filled.VisibilityOff,
                        contentDescription = "Toggle password visibility"
                    )
                }
            },
            visualTransformation = when {
                passwordVisible -> VisualTransformation.None
                else -> PasswordVisualTransformation()
            },
        )

        Spacer(modifier = Modifier.height(Dp16))

        // Signup Button
        Button(
            text = stringResource(id = R.string.ActionResigter),
            type = Primary,
        ) {
            if (name.isNotEmpty() && email.isNotEmpty() && phoneNumber.isNotEmpty() && password.isNotEmpty()) {
                val request = CreateUserRequest(name, email, phoneNumber, password)
                postRegister(request)
            } else {
                Toast.makeText(context, "Harap lengkapi form!", LENGTH_SHORT).show()
            }
        }
        Text(text = "Kembali ke Login",
            style = typography.Underline1,
            modifier = Modifier.padding(vertical = Dp8)
                .clickable { navController.popBackStack() })
    }
}