package com.imaisnaini.kalenderku.ui.screen

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons.Default
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Custom
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Secondary
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown100
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.AVATAR
import com.imaisnaini.kalenderku.ui.util.ImageSize.SIllustration
import com.imaisnaini.kalenderku.ui.viewmodel.MainViewModel
import com.imaisnaini.kalenderku.util.Screen
import com.imaisnaini.kalenderku.util.Screen.Login

@Composable
fun ProfileScreen(
    navController: NavController,
    viewModel: MainViewModel = hiltViewModel()
) = with(viewModel) {
    if (loginToken.isNullOrEmpty()){
        navController.navigate(Login.route) {
            popUpTo(navController.graph.startDestinationId) {
                inclusive = true
            }
        }
    }

    Column(horizontalAlignment = CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(Dp16)
    ) {
        Spacer(modifier = Modifier.height(Dp8))
        Icon(
            imageVector = Default.Person,
            contentDescription = AVATAR,
            modifier = Modifier
                .width(SIllustration.width)
                .height(SIllustration.width)
        )
        TextField(
            value = activeUser?.fullname.orEmpty(),
            label = { Text(text = stringResource(id = R.string.TextFullName), style = typography.Label2) },
            onValueChange = {},
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Brown100
            ),
            modifier = Modifier.padding(bottom = Dp8)
        )
        TextField(
            value = activeUser?.email.orEmpty(),
            label = { Text(text = stringResource(id = R.string.TextEmail), style = typography.Label2) },
            onValueChange = {},
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Brown100
            ),
            modifier = Modifier.padding(bottom = Dp8)
        )
        TextField(
            value = activeUser?.phone.orEmpty(),
            label = { Text(text = stringResource(id = R.string.TextPhoneNumber), style = typography.Label2) },
            onValueChange = {},
            readOnly = true,
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Brown100
            ),
            modifier = Modifier.padding(bottom = Dp8)
        )
        Spacer(modifier = Modifier.weight(1f))
        Button(
            text = stringResource(id = R.string.ActionUpdateProfile),
            type = Secondary,
            modifier = Modifier.fillMaxWidth()
        ) {
            navController.navigate(Screen.EditProfile.route)
        }

        Spacer(modifier = Modifier.height(Dp8))
        Button(
            text = stringResource(id = R.string.ActionLogout),
            type = Custom(
                container = White,
                content = Brown800
            ),
            modifier = Modifier.fillMaxWidth()
        ) {
            logout {
                navController.navigate(Login.route) {
                    navController.popBackStack(Login.route, true)
                }
            }
        }
    }
}