package com.imaisnaini.kalenderku.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.remote.repository.Repository
import com.imaisnaini.kalenderku.bl.data.remote.request.event.CreateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.DeleteEventReqeust
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.event.EventResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EventViewModel @Inject constructor(
    private val repo: Repository
): ViewModel() {
    private val _eventResponse: MutableLiveData<ApiResult<BaseDTO<EventResponse>>> = MutableLiveData()
    val eventResponse = _eventResponse

    private var requestSuccessState: MutableLiveData<Boolean?> = MutableLiveData(null)
    val getRequestSuccessState = requestSuccessState

    val event = MutableLiveData<Event?>()

    private val dialogState = MutableLiveData(false)
    val getDialogState = dialogState
    fun setDialogState(state: Boolean) {
        dialogState.value = state
    }

    fun getEventDetail(eventID: String) = viewModelScope.launch {
        repo.getEventDetail(eventID).collect {
            when(it) {
                is Success -> { event.postValue(it.data?.data?.mapToLocal()) }
                else -> {}
            }
        }
    }

    fun postCreateEvent(request: CreateEventRequest) = viewModelScope.launch {
        repo.createEvent(request).collect {
            when(it) {
                is Success -> { requestSuccessState.postValue(true) }
                else -> { requestSuccessState.postValue(false) }
            }
        }
    }

    fun postUpdateEvent(updateEventRequest: UpdateEventRequest) = viewModelScope.launch {
        repo.updateEvent(updateEventRequest).collect {
            when(it) {
                is Success -> { requestSuccessState.postValue(true) }
                else -> { requestSuccessState.postValue(false) }
            }
        }
    }

    fun postDeleteEvent(eventID: String) = viewModelScope.launch {
        repo.deleteEvent(DeleteEventReqeust(eventID = eventID)).collect {
            when(it) {
                is Success -> { requestSuccessState.postValue(true) }
                else -> { requestSuccessState.postValue(false) }
            }
        }
    }
}