package com.imaisnaini.kalenderku.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.repository.Repository
import com.imaisnaini.kalenderku.bl.data.remote.request.user.CreateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.LoginRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.user.UpdateUserRequest
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.user.LoginResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.user.UserResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repo: Repository
): ViewModel() {
    private val _loginResponse: MutableLiveData<ApiResult<BaseDTO<LoginResponse>>> = MutableLiveData()
    val loginResponse = _loginResponse

    private val _registerResponse: MutableLiveData<ApiResult<BaseDTO<UserResponse>>> = MutableLiveData()
    val registerResponse = _registerResponse

    private val _updateUserReponse: MutableLiveData<ApiResult<BaseDTO<Any>>> = MutableLiveData()
    val updateUserState = _updateUserReponse

    var loginToken: String?
    var activeUser: User?

    init {
        activeUser = repo.loadUser()
        loginToken = repo.loadToken()
    }

    fun postLogin(request: LoginRequest) = viewModelScope.launch {
        repo.login(request).collect {
            _loginResponse.value = it
        }
    }

    fun saveLogin(token: String, user: User) = repo.saveLogin(token, user)

    fun logout(onSuccess: () -> Unit) {
        repo.logOut()
        if (repo.loadUser() == null){
            activeUser = null
            loginToken = null
            onSuccess()
        }
    }

    fun postRegister(request: CreateUserRequest) = viewModelScope.launch {
        repo.createUser(request).collect {
            _registerResponse.value = it
        }
    }

    fun postUpdateUser(request: UpdateUserRequest) = viewModelScope.launch {
        repo.updateUser(request).collect {
            _updateUserReponse.value = it
            when(it) {
                is Success -> {
                    val newUser = request.mapToLocal(activeUser?.userID.orEmpty())
                    saveLogin(loginToken.orEmpty(), newUser)
                }
                else -> {}
            }
        }
    }
}