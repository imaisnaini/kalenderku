package com.imaisnaini.kalenderku.ui.component

import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.Dp
import com.imaisnaini.kalenderku.ui.component.ChipAttr.ChipSize.Medium
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp0
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp1
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp20
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp22
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp24
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp3
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp4
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp6
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING

object ChipAttr {
    data class ChipItem<T>(
        val label: String,
        val value: T,
        val size: ChipSize = Medium,
        val startIcon: ImageVector? = null,
        val endIcon: ImageVector? = null,
        val isDisabled: Boolean = false,
        val favLabel: String = EMPTY_STRING
    )

    sealed class ChipSize(
        val minHeight: Dp = Dp22,
        val iconSize: Dp = Dp24,
        val gapSize: Dp = Dp4,
        val horizontalPadding: Dp = Dp16,
        val verticalPadding: Dp = Dp6,
        val borderSize: Dp = Dp1,
        val borderRadiusSize: Dp = Dp16,
        val favIndentSize: Dp = Dp8,
        val favHorizontalPadding: Dp = Dp8,
        val favVerticalPadding: Dp = Dp0,
        val favBorderRadiusSize: Dp = Dp8
    ) {

        object Medium : ChipSize()

        data class Small(val hasIcon: Boolean) : ChipSize(
            minHeight = Dp20,
            gapSize = Dp8,
            borderRadiusSize = Dp24,
            verticalPadding = if (hasIcon) Dp3 else Dp6
        )
    }
}