package com.imaisnaini.kalenderku.ui.screen

import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.End
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.KeyboardOptions.Companion.Default
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme.colorScheme
import androidx.compose.material3.MaterialTheme.shapes
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale.Companion.Crop
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.ImeAction.Companion.Done
import androidx.compose.ui.text.input.ImeAction.Companion.Next
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType.Companion.Email
import androidx.compose.ui.text.input.KeyboardType.Companion.Password
import androidx.compose.ui.text.input.KeyboardType.Companion.Phone
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextAlign.Companion.Start
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.api.ApiResult.Error
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.remote.request.user.UpdateUserRequest
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp120
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp4
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.util.Constant.ContentDescription.START_ICON
import com.imaisnaini.kalenderku.ui.viewmodel.MainViewModel
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Constants.ONE_FLOAT
import com.imaisnaini.kalenderku.util.Screen

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun EditProfileScreen(
    navController: NavController,
    viewModel: MainViewModel = hiltViewModel()
) = with(viewModel) {
    var name by remember { mutableStateOf(activeUser?.fullname.orEmpty()) }
    var email by remember { mutableStateOf(activeUser?.email.orEmpty()) }
    var phoneNumber by remember { mutableStateOf(activeUser?.phone.orEmpty()) }
    var password by remember { mutableStateOf(activeUser?.password.orEmpty()) }
    var confirmPassword by remember { mutableStateOf(EMPTY_STRING) }

    var passwordErrorState by remember { mutableStateOf(false) }
    var isPasswordMatch by remember { mutableStateOf(true) }
    var isPasswordVisible by remember { mutableStateOf(false) }
    var isConfirmPasswordVisible by remember { mutableStateOf(false) }

    val context = LocalContext.current
    val userState = updateUserState.observeAsState().value

    LaunchedEffect(userState) {
        userState.let {
            when (it) {
                is Success -> {
                    Toast.makeText(context, "Update Success", LENGTH_SHORT).show()
                    navController.navigateUp()
                }

                is Error -> Toast.makeText(context, it.error, LENGTH_SHORT).show()
                else -> {}
            }
        }
    }

    Column(horizontalAlignment = CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(Dp16)
    ) {
        // Profile Picture
        Box(
            modifier = Modifier
                .size(Dp120)
                .background(colorScheme.primary)
                .clip(shapes.medium)
        ) {
            Icon(
                imageVector = Icons.Default.Person,
                contentDescription = null,
                tint = White,
                modifier = Modifier.fillMaxSize()
            )
        }

        Spacer(modifier = Modifier.height(Dp16))

        // Name TextField
        OutlinedTextField(
            value = name,
            onValueChange = { name = it },
            label = { Text(stringResource(id = R.string.TextFullName)) },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Person,
                    contentDescription = START_ICON
                )
            },
            singleLine = true,
            keyboardOptions = KeyboardOptions(imeAction = Next),
            isError = name.isEmpty()
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Email TextField
        OutlinedTextField(
            value = email,
            onValueChange = { email = it },
            label = { Text(stringResource(id = R.string.TextEmail)) },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Email,
                    contentDescription = START_ICON
                )
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = Email,
                capitalization = KeyboardCapitalization.None,
                imeAction = Next
            ),
            singleLine = true,
            isError = email.isEmpty()
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Phone Number TextField
        OutlinedTextField(
            value = phoneNumber,
            onValueChange = { phoneNumber = it },
            label = { Text(stringResource(id = R.string.TextPhoneNumber)) },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Phone,
                    contentDescription = START_ICON
                )
            },
            keyboardOptions = KeyboardOptions(
                keyboardType = Phone,
                capitalization = KeyboardCapitalization.None,
                imeAction = Next
            ),
            singleLine = true,
            isError = phoneNumber.isEmpty()
        )

        Spacer(modifier = Modifier.height(Dp8))

        // Password TextField
        OutlinedTextField(
            value = password,
            onValueChange = {
                password = it
                if(it.isNotEmpty()) {
                    isPasswordMatch = it.equals(confirmPassword)
                }
            },
            label = { Text(stringResource(id = R.string.TextPassword)) },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Lock,
                    contentDescription = START_ICON
                )
            },
            trailingIcon = {
                IconButton(
                    onClick = { isPasswordVisible = !isPasswordVisible }
                ) {
                    Icon(
                        if (isPasswordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff,
                        contentDescription = "Toggle password visibility"
                    )
                }
            },
            visualTransformation = if (isPasswordVisible) VisualTransformation.None
            else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(
                keyboardType = Password,
                capitalization = KeyboardCapitalization.None,
                imeAction = Next
            ),
            singleLine = true,
            isError = passwordErrorState
        )

        Spacer(modifier = Modifier.height(Dp16))

        // Confirm Password
        OutlinedTextField(
            value = confirmPassword,
            onValueChange = {
                confirmPassword = it
                if(it.isNotEmpty()) {
                    isPasswordMatch = it.equals(password)
                }
            },
            label = { Text("Confirm Password") },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.Lock,
                    contentDescription = START_ICON
                )
            },
            trailingIcon = {
                IconButton(
                    onClick = { isConfirmPasswordVisible = !isConfirmPasswordVisible }
                ) {
                    Icon(
                        if (isConfirmPasswordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff,
                        contentDescription = "Toggle password visibility"
                    )
                }
            },
            visualTransformation = if (isConfirmPasswordVisible) VisualTransformation.None
            else PasswordVisualTransformation(),
            keyboardOptions = Default.copy(
                keyboardType = Password,
                imeAction = Done
            ),
            keyboardActions = KeyboardActions(
                onDone = { /* Handle keyboard done action */ }
            ),
            singleLine = true,
            isError = isPasswordMatch.not(),
        )
        // Display error message if passwords do not match
        if (isPasswordMatch.not()) {
            Text(
                text = "Passwords do not match",
                color = Red,
                style = typography.Label1,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = Dp4),
                textAlign = Start
            )
        }


        Spacer(modifier = Modifier.weight(ONE_FLOAT))

        Row(horizontalArrangement = End,
            modifier = Modifier.fillMaxWidth()) {
            Button(
                text = stringResource(id = R.string.ActionCancel),
                type = ButtonAttr.ButtonType.Custom(
                    container = White,
                    content = Brown800
                ),
            ) { navController.navigateUp() }
            Spacer(modifier = Modifier.width(Dp8))
            Button(
                text = stringResource(id = R.string.ActionUpdate),
                type = Primary,
            ) {
                passwordErrorState = (password.isEmpty() || confirmPassword.isEmpty())
                if (passwordErrorState.not() && isPasswordMatch) {
                    val request = UpdateUserRequest(
                        email = email,
                        phone = phoneNumber,
                        fullname = name,
                        newPassword = password,
                        confirmPassword = confirmPassword
                    )
                    postUpdateUser(request)
                }
            }
        }
    }
}