@file:OptIn(ExperimentalFoundationApi::class)

package com.imaisnaini.kalenderku.ui.screen

import android.graphics.Color.*
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons.Filled
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.Holiday
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateStatusUserEventRequest
import com.imaisnaini.kalenderku.ui.MainActivity
import com.imaisnaini.kalenderku.ui.navigation.NavArgs.EVENT_ARGS
import com.imaisnaini.kalenderku.ui.screen.component.DayContent
import com.imaisnaini.kalenderku.ui.screen.component.EventListItem
import com.imaisnaini.kalenderku.ui.screen.component.HolidayColumn
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp1
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp16
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.viewmodel.HomeViewModel
import com.imaisnaini.kalenderku.util.Constants.ONE_FLOAT
import com.imaisnaini.kalenderku.util.Constants.ONE_LONG
import com.imaisnaini.kalenderku.util.DateTimeUtil.LONG_MONTH_YEAR
import com.imaisnaini.kalenderku.util.DateTimeUtil.formatDate
import com.imaisnaini.kalenderku.util.Screen
import io.github.boguszpawlowski.composecalendar.StaticCalendar
import io.github.boguszpawlowski.composecalendar.header.MonthState
import java.time.LocalDate

@Composable
fun HomeScreen(
    navController: NavController,
    viewModel: HomeViewModel = hiltViewModel()
) = with(viewModel) {
    if (activeUser == null){
        navController.navigate(Screen.Login.route) {
            popUpTo(navController.graph.startDestinationId) {
                inclusive = true
            }
        }
    }

    LaunchedEffect(activeUser) {
        getEvent()
        getHoliday()
    }

    val isRequestSuccess = getRequestSuccessState.observeAsState()
    // recall event after success update status
    if (isRequestSuccess.value == true) { getEvent() }

    val activity = LocalContext.current as MainActivity
    val events = remember { mutableStateListOf<Event>() }
    eventList.observe(activity) {
        events.apply {
            clear()
            addAll(it)
        }
    }

    val holidays = remember { mutableStateListOf<Holiday>() }
    holidayList.observe(activity) {
        holidays.apply {
            clear()
            addAll(it)
        }
    }

    val currentDate = remember { mutableStateOf(LocalDate.now()) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(top = Dp8, start = Dp16, end = Dp16),
        horizontalAlignment = CenterHorizontally
    ) {
        StaticCalendar (
            modifier = Modifier.border(border = BorderStroke(Dp1, Brown300)),
            dayContent = { DayContent(dayState = it, events = events, holidays = holidays) },
            monthHeader = {
                Header(monthState = it, date = currentDate, viewModel = viewModel)
            }
        )
        holidays.let {
            HolidayColumn(
                holidays = holidays,
                month = currentDate.value.month,
                year = currentDate.value.year
            )
        }
        Spacer(modifier = Modifier.height(Dp8))
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .weight(ONE_FLOAT)
        ) {
            items(events) { item ->
                EventListItem(
                    item,
                    onActionClick = { status ->
                        val request = UpdateStatusUserEventRequest(
                            eventID = item.eventID,
                            status = status
                        )
                        postEventStatus(request)
                    },
                    onSelected = {
                        navController.navigate(Screen.EditEvent.route.replace("{$EVENT_ARGS}", item.eventID))
                    }
                )
            }
        }
    }
}

@Composable
private fun Header(
    monthState: MonthState,
    date: MutableState<LocalDate>,
    viewModel: HomeViewModel
) = with(monthState) {
    Row(horizontalArrangement = Center,
        verticalAlignment = CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .background(color = Brown300)
    ) {
        IconButton(
            enabled = currentMonth < maxMonth,
            onClick = {
                date.value = date.value.minusMonths(ONE_LONG)
                currentMonth = currentMonth.minusMonths(ONE_LONG)
                viewModel.getEvent(date.value)
            }
        ) {
            Icon(
                imageVector = Filled.ChevronLeft,
                contentDescription = "Back"
            )
        }
        Text(
            text = date.value.formatDate(LONG_MONTH_YEAR),
            modifier = Modifier
                .weight(ONE_FLOAT)
                .align(CenterVertically),
            style = typography.H3
        )
        IconButton(
            enabled = monthState.currentMonth < monthState.maxMonth,
            onClick = {
                date.value = date.value.plusMonths(ONE_LONG)
                currentMonth = currentMonth.plusMonths(ONE_LONG)
                viewModel.getEvent(date.value)
            }
        ) {
            Icon(
                imageVector = Filled.ChevronRight,
                contentDescription = "Next"
            )
        }
    }
}