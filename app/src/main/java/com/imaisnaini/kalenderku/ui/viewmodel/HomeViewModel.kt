package com.imaisnaini.kalenderku.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.imaisnaini.kalenderku.bl.api.ApiResult
import com.imaisnaini.kalenderku.bl.api.ApiResult.Success
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.Holiday
import com.imaisnaini.kalenderku.bl.data.domain.model.User
import com.imaisnaini.kalenderku.bl.data.remote.repository.Repository
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateStatusUserEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.response.BaseDTO
import com.imaisnaini.kalenderku.bl.data.remote.response.event.EventResponse
import com.imaisnaini.kalenderku.bl.data.remote.response.holiday.HolidayResponse
import com.imaisnaini.kalenderku.util.DateTimeUtil.SHORT_MONTH_YEAR
import com.imaisnaini.kalenderku.util.DateTimeUtil.formatDate
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repo: Repository
): ViewModel() {
    private val _eventResponse: MutableLiveData<ApiResult<BaseDTO<List<EventResponse>>>> = MutableLiveData()
    val eventResponse = _eventResponse

    private val _holidayResponse: MutableLiveData<ApiResult<BaseDTO<List<HolidayResponse>>>> = MutableLiveData()
    val holidayResponse = _holidayResponse

    val activeUser: User? = repo.loadUser()
    val eventList = MutableLiveData<List<Event>>(emptyList())
    val holidayList = MutableLiveData<List<Holiday>>(emptyList())

    private var requestSuccessState: MutableLiveData<Boolean?> = MutableLiveData(null)
    val getRequestSuccessState = requestSuccessState

    fun getEvent(date: LocalDate = LocalDate.now()) = viewModelScope.launch {
        repo.getEvent(
            activeUser?.userID.orEmpty(),
            date.formatDate(SHORT_MONTH_YEAR)
        ).collect {
            _eventResponse.value = it
            when(it) {
                is Success -> {
                    eventList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }

                else -> {}
            }
        }
    }
    fun getHoliday() = viewModelScope.launch {
        repo.getHoliday().collect {
            _holidayResponse.value = it
            when(it) {
                is Success -> {
                    holidayList.postValue(it.data?.data.orEmpty().map { x -> x.mapToLocal() })
                }

                else -> {}
            }
        }
    }

    fun postEventStatus(request: UpdateStatusUserEventRequest) = viewModelScope.launch {
        repo.updateStatusEvent(request).collect {
            when(it) {
                is Success -> { requestSuccessState.postValue(true) }
                else -> { requestSuccessState.postValue(false) }
            }
        }
    }
}