package com.imaisnaini.kalenderku.ui.screen

import android.annotation.SuppressLint
import android.graphics.Color.parseColor
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement.Center
import androidx.compose.foundation.layout.Arrangement.End
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.AlertDialog
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.Icons.Filled
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.DatePicker
import androidx.compose.material3.DatePickerDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults.centerAlignedTopAppBarColors
import androidx.compose.material3.rememberDatePickerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign.Companion.Start
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.imaisnaini.kalenderku.R
import com.imaisnaini.kalenderku.bl.data.domain.model.Event
import com.imaisnaini.kalenderku.bl.data.domain.model.Friend
import com.imaisnaini.kalenderku.bl.data.remote.request.event.CreateEventRequest
import com.imaisnaini.kalenderku.bl.data.remote.request.event.UpdateEventRequest
import com.imaisnaini.kalenderku.ui.MainActivity
import com.imaisnaini.kalenderku.ui.component.Button
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Custom
import com.imaisnaini.kalenderku.ui.component.ButtonAttr.ButtonType.Primary
import com.imaisnaini.kalenderku.ui.component.Chip
import com.imaisnaini.kalenderku.ui.component.ChipAttr.ChipItem
import com.imaisnaini.kalenderku.ui.component.RowToggleButtonGroup
import com.imaisnaini.kalenderku.ui.theme.BWTones.GraniteGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.LightGray
import com.imaisnaini.kalenderku.ui.theme.BWTones.White
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown300
import com.imaisnaini.kalenderku.ui.theme.Browns.Brown800
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Blue
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Green
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Magenta
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Orange
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Pink
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Purple
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Red
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Turquoise
import com.imaisnaini.kalenderku.ui.theme.CalenderColor.Yellow
import com.imaisnaini.kalenderku.ui.theme.Dimen
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp0
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp1
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp4
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp40
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp50
import com.imaisnaini.kalenderku.ui.theme.Dimen.Dp8
import com.imaisnaini.kalenderku.ui.theme.KalenderkuTheme.typography
import com.imaisnaini.kalenderku.ui.viewmodel.EventViewModel
import com.imaisnaini.kalenderku.ui.viewmodel.FriendViewModel
import com.imaisnaini.kalenderku.util.Constants.EMPTY_STRING
import com.imaisnaini.kalenderku.util.Constants.ONE
import com.imaisnaini.kalenderku.util.Constants.ONE_FLOAT
import com.imaisnaini.kalenderku.util.Constants.ZERO
import com.imaisnaini.kalenderku.util.DateTimeUtil.DATE_MONTH_YEAR
import com.imaisnaini.kalenderku.util.DateTimeUtil.LONG_DATE_FORMAT
import com.imaisnaini.kalenderku.util.extentions.orTrue
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.util.Date

@SuppressLint("SimpleDateFormat")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddEventScreen(
    navController: NavController,
    eventID: String? = null,
    viewModel: EventViewModel = hiltViewModel(),
    friendViewModel: FriendViewModel = hiltViewModel()
) = with(viewModel) {
    val colorPallete = listOf(
        Green,
        Yellow,
        Orange,
        Red,
        Blue,
        Pink,
        Magenta,
        Purple,
        Turquoise,
        Blue
    )
    var colorExpanded by remember { mutableStateOf(false) }
    var eventColor by remember { mutableStateOf(LightGray) }
    val activity = LocalContext.current as MainActivity

    var tanggal by remember { mutableStateOf(Date()) }
    var nama by remember { mutableStateOf(EMPTY_STRING) }
    var deskripsi by remember { mutableStateOf(EMPTY_STRING) }
    var notification by remember { mutableStateOf(true) }
    var privacy by remember { mutableStateOf(true) }

    var friendExpanded by remember { mutableStateOf(false) }
    val friends = remember { mutableStateListOf<Friend>() }
    val invitedFriends = remember { mutableStateListOf<Friend>() }
    val friendChip = remember { mutableStateListOf<ChipItem<Friend>>() }
    friendViewModel.friendList.observe(activity) {
        friends.apply {
            clear()
            addAll(it)
        }
    }

    LaunchedEffect(eventID) {
        if (eventID != null) {
            getEventDetail(eventID)
        }
    }
    var activeEvent by remember {
        mutableStateOf<Event?>(null)
    }
    var triggerChange by remember {
        mutableStateOf(true)
    }
    event.observe(activity) {
        if(event.value != null && triggerChange){ // use trigger to avoid recomposition
            // assign event value for edit mode
            triggerChange = false
            activeEvent = event.value
            tanggal = Date.from(activeEvent?.eventDate?.atStartOfDay(ZoneId.systemDefault())?.toInstant())
            eventColor = Color(parseColor(activeEvent?.color))
            nama = activeEvent?.name.orEmpty()
            deskripsi = activeEvent?.description.orEmpty()
            notification = activeEvent?.isNotification.orTrue()
            privacy = activeEvent?.isPrivacy.orTrue()
            activeEvent?.friends?.forEach {
                val item = it.mapToFriend()
                val existingFriendIndex = friendChip.indexOfFirst { it.value == item }

                if (existingFriendIndex == -1) {
                    // Friend doesn't exist, add it
                    friendChip.add(
                        ChipItem(
                            label = item.fullname,
                            value = item
                        )
                    )
                }

                if (invitedFriends.contains(item).not()) invitedFriends.add(item)
            }
        }
    }

    // Dialog state
    val datePickerState = rememberDatePickerState()
    val datePickerDialog = remember { mutableStateOf(false) }


    val isRequestSuccess = getRequestSuccessState.observeAsState().value
    val context = LocalContext.current
    var isToastShown by remember {
        mutableStateOf(false)
    }
    // recall event after success update status
    if (isRequestSuccess == true) {
        navController.navigateUp()
        if (isToastShown.not()) {
            Toast.makeText(context, "Event Disimpan", LENGTH_SHORT).show()
            isToastShown = true
        }
    }

    val dialogState = remember { mutableStateOf(false) }
    getDialogState.observe(activity) {
        dialogState.value = it
    }

    Column {
        CenterAlignedTopAppBar(
            title = {
                Text(
                    stringResource(id = if(activeEvent == null) R.string.ScreenAddEvent else R.string.ScreenEditEvent),
                    color = White,
                    style = typography.H3
                )
            },
            colors = centerAlignedTopAppBarColors(
                containerColor = Brown800,
                titleContentColor = White,
                navigationIconContentColor = White
            ),
            navigationIcon = {
                IconButton(onClick = { navController.navigateUp() }) {
                    Icon(
                        imageVector = Filled.ArrowBack,
                        contentDescription = "Back",
                        tint = White
                    )
                }
            }
        )
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimen.Dp16),
            horizontalAlignment = CenterHorizontally
        ) {
            if (dialogState.value) {
                DeleteDialog(
                    onDismiss = { setDialogState(false) },
                    onConfirm = {
                        postDeleteEvent(eventID.orEmpty())
                        setDialogState(false)
                    }
                )
            }
            OutlinedTextField(
                value = SimpleDateFormat(LONG_DATE_FORMAT).format(tanggal),
                onValueChange = {},
                label = { Text("Tanggal") },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = Dp8),
                trailingIcon = {
                    Icon(
                        imageVector = Icons.Default.DateRange,
                        contentDescription = "Select date",
                        modifier = Modifier.clickable {
                            datePickerDialog.value = true
                        }
                    )
                },
                readOnly = true
            )

            OutlinedTextField(
                value = nama,
                onValueChange = { nama = it },
                label = { Text("Nama") },
                singleLine = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = Dp8),
                keyboardOptions = KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Next
                ),
                isError = nama.isEmpty()
            )

            // Text area for Deskripsi field
            OutlinedTextField(
                value = deskripsi,
                onValueChange = { deskripsi = it },
                label = { Text("Deskripsi") },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = Dp8),
                maxLines = 3,
                isError = deskripsi.isEmpty()
            )

            // Text label above the box to trigger dropdown menu
            Column(
                horizontalAlignment = CenterHorizontally,
                modifier = Modifier
                    .padding(vertical = Dp8)
                    .clickable { colorExpanded = !colorExpanded } // Toggle expanded state
            ) {
                Text(
                    text = "Warna",
                    textAlign = Start,
                    modifier = Modifier.fillMaxWidth()
                )
                Box(
                    modifier = Modifier
                        .background(color = eventColor, shape = RoundedCornerShape(Dp4))
                        .fillMaxWidth()
                        .height(Dp40)
                )
            }
            DropdownMenu(
                expanded = colorExpanded,
                onDismissRequest = { colorExpanded = false },
                modifier = Modifier
                    .background(color = White)
                    .padding(bottom = Dp8),
            ) {
                colorPallete.forEach { item ->
                    DropdownMenuItem(
                        onClick = {
                            eventColor = item
                            colorExpanded = false
                        }
                    ) {
                        Box(
                            modifier = Modifier
                                .background(color = item, shape = RoundedCornerShape(Dp4))
                                .size(Dp40)
                        )
                    }
                }
            }

            Text(
                "Pemberitahuan",
                textAlign = Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = Dp8)
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = Dp8)
            ) {
                RowToggleButtonGroup(
                    modifier = Modifier,
                    buttonCount = 2,
                    selectedColor = Brown300,
                    unselectedColor = Transparent,
                    elevation = ButtonDefaults.elevation(Dp0), // elevation of toggle group buttons
                    buttonTexts = arrayOf("Ya", "Tidak"),
                    buttonHeight = Dp40,
                    primarySelection = (if (notification) ZERO else ONE)
                ) { index ->
                    notification = (index == ZERO)
                }
            }

            Text(
                "Privasi",
                textAlign = Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = Dp8)
            )
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = Dp8),
            ) {
                RowToggleButtonGroup(
                    modifier = Modifier,
                    buttonCount = 2,
                    selectedColor = Brown300,
                    unselectedColor = Transparent,
                    elevation = ButtonDefaults.elevation(Dp0), // elevation of toggle group buttons
                    buttonTexts = arrayOf("Ya", "Tidak"),
                    buttonHeight = Dp40,
                    primarySelection = (if (privacy) ZERO else ONE)
                ) { index ->
                    privacy = (index == ZERO)
                }
            }

            Text(
                "Teman",
                textAlign = Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = Dp8)
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(Dp50)
                    .border(
                        border = BorderStroke(Dp1, GraniteGray),
                        shape = RoundedCornerShape(Dp4)
                    )
                    .clickable {
                        if (activeEvent == null) friendExpanded =
                            !friendExpanded // only enabled when add mode
                    },
                verticalArrangement = Center
            ) {
                Column(modifier = Modifier.padding(Dp4)) {
                    Chip(
                        items = friendChip,
                        selectedItems = invitedFriends
                    ) {
                        if (friendChip.contains(it)) {
                            friendChip.remove(it)
                            invitedFriends.remove(it.value)
                        }
                    }
                }
            }
            DropdownMenu(
                expanded = friendExpanded,
                onDismissRequest = { friendExpanded = false },
                modifier = Modifier
                    .background(color = White)
                    .padding(bottom = Dp8),
            ) {
                friends.forEach { item ->
                    DropdownMenuItem(
                        onClick = {
                            if (friendChip.isEmpty()) {
                                friendChip.add(ChipItem(item.fullname, item))
                                invitedFriends.add(item)
                            } else {
                                val existingFriendIndex =
                                    friendChip.indexOfFirst { it.value == item }

                                if (existingFriendIndex != -1) {
                                    // Friend exists, remove it
                                    friendChip.removeAt(existingFriendIndex)
                                } else {
                                    // Friend doesn't exist, add it
                                    friendChip.add(
                                        ChipItem(
                                            label = item.fullname,
                                            value = item,
                                            endIcon = Icons.Default.Clear
                                        )
                                    )
                                }

                                if (invitedFriends.contains(item)) invitedFriends.remove(item)
                                else invitedFriends.add(item)
                            }
                            friendExpanded = false
                        }
                    ) {
                        Text(item.fullname)
                    }
                }
            }

            Spacer(Modifier.weight(ONE_FLOAT))
            Row(
                horizontalArrangement = End,
                modifier = Modifier.fillMaxWidth()
            ) {
                if(activeEvent != null) {
                    Button(
                        text = stringResource(id = R.string.ActionDelete),
                        type = Custom(
                            container = White,
                            content = Red,
                            outline = Red
                        ),
                        startIcon = Filled.DeleteForever
                    ) {
                        setDialogState(true)
                    }
                    Spacer(modifier = Modifier.width(Dp8))
                }
                Button(
                    text = stringResource(id = R.string.ActionSave),
                    type = Primary,
                ) {
                    if (nama.isNotEmpty() && deskripsi.isNotEmpty()) {
                        if (activeEvent == null) {
                            val request = CreateEventRequest(
                                name = nama,
                                description = deskripsi,
                                isPrivacy = privacy,
                                isNotification = notification,
                                color = colorToHexString(eventColor),
                                eventDate = SimpleDateFormat(DATE_MONTH_YEAR).format(tanggal),
                                friendIDs = invitedFriends.map { x -> x.friendUserID }
                            )
                            postCreateEvent(request)
                        } else {
                            val request = UpdateEventRequest(
                                eventID = eventID.orEmpty(),
                                name = nama,
                                description = deskripsi,
                                isPrivacy = privacy,
                                isNotification = notification,
                                color = colorToHexString(eventColor),
                                eventDate = SimpleDateFormat(DATE_MONTH_YEAR).format(tanggal)
                            )
                            postUpdateEvent(request)
                        }
                    }
                }
            }
            // Date Picker Dialog
            if (datePickerDialog.value) {
                DatePickerDialog(
                    onDismissRequest = { datePickerDialog.value = false },
                    confirmButton = {
                        TextButton(
                            onClick = {
                                datePickerDialog.value = false
                                tanggal =
                                    datePickerState.selectedDateMillis?.let { Date(it) } ?: Date()
                            }
                        ) {
                            Text(stringResource(id = R.string.ActionSave))
                        }
                    },
                    dismissButton = {
                        TextButton(
                            onClick = { datePickerDialog.value = false }
                        ) {
                            Text(stringResource(id = R.string.ActionCancel))
                        }
                    }
                ) {
                    DatePicker(state = datePickerState)
                }
            }
        }
    }
}

@Composable
fun DeleteDialog(
    onDismiss: () -> Unit,
    onConfirm: () -> Unit
) {
    AlertDialog(
        title = { Text(text = "Hapus Event") },
        text = { Text(text = "Anda yakin ingin menghapus event?") },
        onDismissRequest = { onDismiss() },
        confirmButton = {
            TextButton(onClick = { onConfirm() }) {
                Text(text = stringResource(id = R.string.ActionDelete))
            }
        },
        dismissButton = {
            TextButton(onClick = { onDismiss() }) {
                Text(text = stringResource(id = R.string.ActionCancel))
            }
        }
    )
}
fun colorToHexString(color: Color): String {
    // Retrieve the color value as an integer
    val colorInt = color.toArgb()

    // Convert the color integer to a hexadecimal string
    return String.format("#%08X", colorInt)
}